
DO $
BEGIN
    IF NOT EXISTS(
        SELECT schema_name
          FROM information_schema.schemata
          WHERE schema_name = 'dest_001'
      )
    THEN
      EXECUTE 'CREATE SCHEMA dest_001';
    END IF;
END $;
        
 DROP TABLE IF EXISTS "dest_001"."Counterparty" CASCADE;
 CREATE TABLE "dest_001"."Counterparty" (
   "user_id" INTEGER PRIMARY KEY,
   "middlename" VARCHAR(100),
   "lastname" VARCHAR(100),
   "national" VARCHAR(100),
   "firstname" VARCHAR(100),
   "company" VARCHAR(100)
 );

 DROP TABLE IF EXISTS "dest_001"."Transaction" CASCADE;
 CREATE TABLE "dest_001"."Transaction" (
   "trans_act_id" INTEGER PRIMARY KEY,
   "approvalDate" VARCHAR(100),
   "transactionDate" VARCHAR(100),
   "authoriser" VARCHAR(100),
   "paymentMethod" VARCHAR(100),
   "accountName" VARCHAR(100)
 );

 DROP TABLE IF EXISTS "dest_001"."join_Counterparty_Transaction" CASCADE;
 CREATE TABLE "dest_001"."join_Counterparty_Transaction" (
   "Counterparty_Transaction_id" INTEGER PRIMARY KEY,
   "trans_act_id" INTEGER,
   "user_id" INTEGER
 );
 ALTER TABLE "dest_001"."join_Counterparty_Transaction" ADD CONSTRAINT FK_Counterparty FOREIGN KEY (user_id) REFERENCES "Counterparty"(user_id);
 ALTER TABLE "dest_001"."join_Counterparty_Transaction" ADD CONSTRAINT FK_Transaction FOREIGN KEY (trans_act_id) REFERENCES "Transaction"(trans_act_id);
