name := "platonic_universe"

version := "1.4.8"

scalaVersion := "2.13.1"

libraryDependencies += "com.typesafe" % "config" % "1.4.0"
libraryDependencies += "org.typelevel" %% "cats-core" % "2.1.0"
libraryDependencies += "org.tpolecat" %% "doobie-core" % "0.8.8"
libraryDependencies += "org.tpolecat" %% "doobie-h2" % "0.8.8"
libraryDependencies += "dev.zio" %% "zio-interop-cats" % "2.0.0.0-RC10"
libraryDependencies += "org.scala-graph" %% "graph-core" % "1.13.2"
libraryDependencies += "com.daodecode" %% "scalax-collection" % "0.3.0"

libraryDependencies += "org.apache.logging.log4j" % "log4j-api" % "2.13.0"
libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.13.0"
libraryDependencies += "io.argonaut" %% "argonaut" % "6.3.1"