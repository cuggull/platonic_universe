
DO $
BEGIN
    IF NOT EXISTS(
        SELECT schema_name
          FROM information_schema.schemata
          WHERE schema_name = 'public'
      )
    THEN
      EXECUTE 'CREATE SCHEMA public';
    END IF;
END $;
        
 DROP TABLE IF EXISTS "public"."Counterparty" CASCADE;
 CREATE TABLE "public"."Counterparty" (
   "user_id" INTEGER PRIMARY KEY,
   "middlename" VARCHAR(100),
   "lastname" VARCHAR(100),
   "national" VARCHAR(100),
   "firstname" VARCHAR(100),
   "company" VARCHAR(100)
 );

 DROP TABLE IF EXISTS "public"."Product" CASCADE;
 CREATE TABLE "public"."Product" (
   "sku_id" INTEGER PRIMARY KEY,
   "trans_act_id" INTEGER,
   "user_id" INTEGER,
   "store" VARCHAR(100),
   "productID" VARCHAR(100),
   "unitValue" VARCHAR(100),
   "productName" VARCHAR(100),
   "quantity" VARCHAR(100)
 );

 DROP TABLE IF EXISTS "public"."Store" CASCADE;
 CREATE TABLE "public"."Store" (
   "store_id" INTEGER PRIMARY KEY,
   "trans_act_id" INTEGER,
   "state" VARCHAR(100),
   "address1" VARCHAR(100),
   "postcode" VARCHAR(100),
   "country" VARCHAR(100),
   "address2" VARCHAR(100)
 );

 DROP TABLE IF EXISTS "public"."Transaction" CASCADE;
 CREATE TABLE "public"."Transaction" (
   "trans_act_id" INTEGER PRIMARY KEY,
   "user_id" INTEGER,
   "approvalDate" VARCHAR(100),
   "transactionDate" VARCHAR(100),
   "authoriser" VARCHAR(100),
   "paymentMethod" VARCHAR(100),
   "accountName" VARCHAR(100)
 );
 ALTER TABLE "public"."Product" ADD CONSTRAINT FK_Counterparty FOREIGN KEY (user_id) REFERENCES "Counterparty"(user_id);
 ALTER TABLE "public"."Product" ADD CONSTRAINT FK_Transaction FOREIGN KEY (trans_act_id) REFERENCES "Transaction"(trans_act_id);
 ALTER TABLE "public"."Store" ADD CONSTRAINT FK_Transaction FOREIGN KEY (trans_act_id) REFERENCES "Transaction"(trans_act_id);
 ALTER TABLE "public"."Transaction" ADD CONSTRAINT FK_Counterparty FOREIGN KEY (user_id) REFERENCES "Counterparty"(user_id);
