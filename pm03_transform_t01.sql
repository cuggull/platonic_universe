SELECT "trans_act_id" ,
"approvalDate","transactionDate","authoriser","paymentMethod","accountName"   FROM (
SELECT "public"."Transaction"."trans_act_id",
"public"."Transaction"."approvalDate","public"."Transaction"."transactionDate","public"."Transaction"."authoriser","public"."Transaction"."paymentMethod","public"."Transaction"."accountName",
 "public"."Counterparty"."user_id" AS "Counterparty_user_id" ,null::integer AS "Product_sku_id" ,null::integer AS "Store_store_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Transaction" INNER JOIN "public"."Counterparty" ON "public"."Counterparty"."user_id" = "public"."Transaction"."user_id"  
 Union 
SELECT "public"."Transaction"."trans_act_id",
"public"."Transaction"."approvalDate","public"."Transaction"."transactionDate","public"."Transaction"."authoriser","public"."Transaction"."paymentMethod","public"."Transaction"."accountName",
 "public"."Counterparty"."user_id" AS "Counterparty_user_id" , "public"."Product"."sku_id" AS "Product_sku_id" ,null::integer AS "Store_store_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Transaction" INNER JOIN "public"."Counterparty" ON "public"."Counterparty"."user_id" = "public"."Transaction"."user_id"  INNER JOIN "public"."Product" ON "public"."Product"."user_id" = "public"."Counterparty"."user_id"  
 Union 
SELECT "public"."Transaction"."trans_act_id",
"public"."Transaction"."approvalDate","public"."Transaction"."transactionDate","public"."Transaction"."authoriser","public"."Transaction"."paymentMethod","public"."Transaction"."accountName",
 "public"."Counterparty"."user_id" AS "Counterparty_user_id" , "public"."Product"."sku_id" AS "Product_sku_id" ,null::integer AS "Store_store_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Transaction" INNER JOIN "public"."Product" ON "public"."Product"."trans_act_id" = "public"."Transaction"."trans_act_id"  INNER JOIN "public"."Counterparty" ON "public"."Counterparty"."user_id" = "public"."Product"."user_id"  
 Union 
SELECT "public"."Transaction"."trans_act_id",
"public"."Transaction"."approvalDate","public"."Transaction"."transactionDate","public"."Transaction"."authoriser","public"."Transaction"."paymentMethod","public"."Transaction"."accountName",
null::integer AS "Counterparty_user_id" ,null::integer AS "Product_sku_id" , "public"."Store"."store_id" AS "Store_store_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Transaction" INNER JOIN "public"."Store" ON "public"."Store"."trans_act_id" = "public"."Transaction"."trans_act_id"  
 Union 
SELECT "public"."Transaction"."trans_act_id",
"public"."Transaction"."approvalDate","public"."Transaction"."transactionDate","public"."Transaction"."authoriser","public"."Transaction"."paymentMethod","public"."Transaction"."accountName",
null::integer AS "Counterparty_user_id" , "public"."Product"."sku_id" AS "Product_sku_id" ,null::integer AS "Store_store_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Transaction" INNER JOIN "public"."Product" ON "public"."Product"."trans_act_id" = "public"."Transaction"."trans_act_id"   ) Plato_Unions_Transaction ;

SELECT "user_id" ,
"middlename","lastname","national","firstname","company"   FROM (
SELECT "public"."Counterparty"."user_id",
"public"."Counterparty"."middlename","public"."Counterparty"."lastname","public"."Counterparty"."national","public"."Counterparty"."firstname","public"."Counterparty"."company",
 "public"."Counterparty"."user_id" AS "Counterparty_user_id" , "public"."Product"."sku_id" AS "Product_sku_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Counterparty" INNER JOIN "public"."Product" ON "public"."Product"."user_id" = "public"."Counterparty"."user_id"  INNER JOIN "public"."Transaction" ON "public"."Transaction"."trans_act_id" = "public"."Product"."trans_act_id"  INNER JOIN "public"."Store" ON "public"."Store"."trans_act_id" = "public"."Transaction"."trans_act_id"  
 Union 
SELECT "public"."Counterparty"."user_id",
"public"."Counterparty"."middlename","public"."Counterparty"."lastname","public"."Counterparty"."national","public"."Counterparty"."firstname","public"."Counterparty"."company",
 "public"."Counterparty"."user_id" AS "Counterparty_user_id" , "public"."Product"."sku_id" AS "Product_sku_id" ,null::integer AS "Transaction_trans_act_id"  FROM "public"."Counterparty" INNER JOIN "public"."Product" ON "public"."Product"."user_id" = "public"."Counterparty"."user_id"  
 Union 
SELECT "public"."Counterparty"."user_id",
"public"."Counterparty"."middlename","public"."Counterparty"."lastname","public"."Counterparty"."national","public"."Counterparty"."firstname","public"."Counterparty"."company",
 "public"."Counterparty"."user_id" AS "Counterparty_user_id" ,null::integer AS "Product_sku_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Counterparty" INNER JOIN "public"."Transaction" ON "public"."Transaction"."user_id" = "public"."Counterparty"."user_id"  INNER JOIN "public"."Store" ON "public"."Store"."trans_act_id" = "public"."Transaction"."trans_act_id"  
 Union 
SELECT "public"."Counterparty"."user_id",
"public"."Counterparty"."middlename","public"."Counterparty"."lastname","public"."Counterparty"."national","public"."Counterparty"."firstname","public"."Counterparty"."company",
 "public"."Counterparty"."user_id" AS "Counterparty_user_id" , "public"."Product"."sku_id" AS "Product_sku_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Counterparty" INNER JOIN "public"."Product" ON "public"."Product"."user_id" = "public"."Counterparty"."user_id"  INNER JOIN "public"."Transaction" ON "public"."Transaction"."trans_act_id" = "public"."Product"."trans_act_id"  
 Union 
SELECT "public"."Counterparty"."user_id",
"public"."Counterparty"."middlename","public"."Counterparty"."lastname","public"."Counterparty"."national","public"."Counterparty"."firstname","public"."Counterparty"."company",
 "public"."Counterparty"."user_id" AS "Counterparty_user_id" , "public"."Product"."sku_id" AS "Product_sku_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Counterparty" INNER JOIN "public"."Transaction" ON "public"."Transaction"."user_id" = "public"."Counterparty"."user_id"  INNER JOIN "public"."Product" ON "public"."Product"."trans_act_id" = "public"."Transaction"."trans_act_id"  
 Union 
SELECT "public"."Counterparty"."user_id",
"public"."Counterparty"."middlename","public"."Counterparty"."lastname","public"."Counterparty"."national","public"."Counterparty"."firstname","public"."Counterparty"."company",
 "public"."Counterparty"."user_id" AS "Counterparty_user_id" ,null::integer AS "Product_sku_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Counterparty" INNER JOIN "public"."Transaction" ON "public"."Transaction"."user_id" = "public"."Counterparty"."user_id"   ) Plato_Unions_Counterparty ;

SELECT "Counterparty_Transaction_id"  ,
"join_Counterparty_Transaction_trans_act_id","join_Counterparty_Transaction_user_id"  FROM (
SELECT  "public"."Counterparty"."user_id" AS "Counterparty_user_id" ,null::integer AS "Product_sku_id" , "public"."Store"."store_id" AS "Store_store_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Counterparty" INNER JOIN "public"."Transaction" ON "public"."Transaction"."user_id" = "public"."Counterparty"."user_id"  INNER JOIN "public"."Store" ON "public"."Store"."trans_act_id" = "public"."Transaction"."trans_act_id"  
 Union 
SELECT  "public"."Counterparty"."user_id" AS "Counterparty_user_id" , "public"."Product"."sku_id" AS "Product_sku_id" ,null::integer AS "Store_store_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Counterparty" INNER JOIN "public"."Product" ON "public"."Product"."user_id" = "public"."Counterparty"."user_id"  INNER JOIN "public"."Transaction" ON "public"."Transaction"."trans_act_id" = "public"."Product"."trans_act_id"  
 Union 
SELECT  "public"."Counterparty"."user_id" AS "Counterparty_user_id" , "public"."Product"."sku_id" AS "Product_sku_id" ,null::integer AS "Store_store_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Transaction" INNER JOIN "public"."Product" ON "public"."Product"."trans_act_id" = "public"."Transaction"."trans_act_id"  INNER JOIN "public"."Counterparty" ON "public"."Counterparty"."user_id" = "public"."Product"."user_id"  
 Union 
SELECT  "public"."Counterparty"."user_id" AS "Counterparty_user_id" , "public"."Product"."sku_id" AS "Product_sku_id" ,null::integer AS "Store_store_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Transaction" INNER JOIN "public"."Counterparty" ON "public"."Counterparty"."user_id" = "public"."Transaction"."user_id"  INNER JOIN "public"."Product" ON "public"."Product"."user_id" = "public"."Counterparty"."user_id"  
 Union 
SELECT  "public"."Counterparty"."user_id" AS "Counterparty_user_id" , "public"."Product"."sku_id" AS "Product_sku_id" , "public"."Store"."store_id" AS "Store_store_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Counterparty" INNER JOIN "public"."Product" ON "public"."Product"."user_id" = "public"."Counterparty"."user_id"  INNER JOIN "public"."Transaction" ON "public"."Transaction"."trans_act_id" = "public"."Product"."trans_act_id"  INNER JOIN "public"."Store" ON "public"."Store"."trans_act_id" = "public"."Transaction"."trans_act_id"  
 Union 
SELECT  "public"."Counterparty"."user_id" AS "Counterparty_user_id" ,null::integer AS "Product_sku_id" ,null::integer AS "Store_store_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Transaction" INNER JOIN "public"."Counterparty" ON "public"."Counterparty"."user_id" = "public"."Transaction"."user_id"  
 Union 
SELECT null::integer AS "Counterparty_user_id" ,null::integer AS "Product_sku_id" , "public"."Store"."store_id" AS "Store_store_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Transaction" INNER JOIN "public"."Store" ON "public"."Store"."trans_act_id" = "public"."Transaction"."trans_act_id"  
 Union 
SELECT  "public"."Counterparty"."user_id" AS "Counterparty_user_id" , "public"."Product"."sku_id" AS "Product_sku_id" ,null::integer AS "Store_store_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Counterparty" INNER JOIN "public"."Transaction" ON "public"."Transaction"."user_id" = "public"."Counterparty"."user_id"  INNER JOIN "public"."Product" ON "public"."Product"."trans_act_id" = "public"."Transaction"."trans_act_id"  
 Union 
SELECT  "public"."Counterparty"."user_id" AS "Counterparty_user_id" ,null::integer AS "Product_sku_id" ,null::integer AS "Store_store_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Counterparty" INNER JOIN "public"."Transaction" ON "public"."Transaction"."user_id" = "public"."Counterparty"."user_id"  
 Union 
SELECT  "public"."Counterparty"."user_id" AS "Counterparty_user_id" , "public"."Product"."sku_id" AS "Product_sku_id" ,null::integer AS "Store_store_id" ,null::integer AS "Transaction_trans_act_id"  FROM "public"."Counterparty" INNER JOIN "public"."Product" ON "public"."Product"."user_id" = "public"."Counterparty"."user_id"  
 Union 
SELECT null::integer AS "Counterparty_user_id" , "public"."Product"."sku_id" AS "Product_sku_id" ,null::integer AS "Store_store_id" , "public"."Transaction"."trans_act_id" AS "Transaction_trans_act_id"  FROM "public"."Transaction" INNER JOIN "public"."Product" ON "public"."Product"."trans_act_id" = "public"."Transaction"."trans_act_id"   ) Plato_Unions_join_Counterparty_Transaction ;
