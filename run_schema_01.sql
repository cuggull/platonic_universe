
DO $
BEGIN
    IF NOT EXISTS(
        SELECT schema_name
          FROM information_schema.schemata
          WHERE schema_name = 'public'
      )
    THEN
      EXECUTE 'CREATE SCHEMA public';
    END IF;
END $;
        
 DROP TABLE IF EXISTS "public"."Aggregate" CASCADE;
 CREATE TABLE "public"."Aggregate" (
   "Aggregate_id" INTEGER PRIMARY KEY
 );

 DROP TABLE IF EXISTS "public"."Deployment" CASCADE;
 CREATE TABLE "public"."Deployment" (
   "Deployment_id" INTEGER PRIMARY KEY,
   "Environment_id" INTEGER,
   "Graph_id" INTEGER,
   "Run_id" INTEGER,
   "SourceConfig_id" INTEGER,
   "name" VARCHAR(100),
   "version" VARCHAR(100),
   "version_FQ" VARCHAR(100),
   "createDateTime" VARCHAR(100),
   "authorityGUID" VARCHAR(100)
 );

 DROP TABLE IF EXISTS "public"."Environment" CASCADE;
 CREATE TABLE "public"."Environment" (
   "Environment_id" INTEGER PRIMARY KEY,
   "name" VARCHAR(100),
   "version" VARCHAR(100),
   "version_FQ" VARCHAR(100),
   "createDateTime" VARCHAR(100),
   "authorityGUID" VARCHAR(100)
 );

 DROP TABLE IF EXISTS "public"."Function" CASCADE;
 CREATE TABLE "public"."Function" (
   "Function_id" INTEGER PRIMARY KEY,
   "InputSet_id" INTEGER,
   "OutputSet_id" INTEGER,
   "name" VARCHAR(100),
   "version" VARCHAR(100),
   "version_FQ" VARCHAR(100),
   "createDateTime" VARCHAR(100),
   "authorityGUID" VARCHAR(100)
 );

 DROP TABLE IF EXISTS "public"."Graph" CASCADE;
 CREATE TABLE "public"."Graph" (
   "Graph_id" INTEGER PRIMARY KEY,
   "Function_id" INTEGER,
   "name" VARCHAR(100),
   "version" VARCHAR(100),
   "version_FQ" VARCHAR(100),
   "createDateTime" VARCHAR(100),
   "authorityGUID" VARCHAR(100)
 );

 DROP TABLE IF EXISTS "public"."InputSet" CASCADE;
 CREATE TABLE "public"."InputSet" (
   "InputSet_id" INTEGER PRIMARY KEY
 );

 DROP TABLE IF EXISTS "public"."OutputSet" CASCADE;
 CREATE TABLE "public"."OutputSet" (
   "OutputSet_id" INTEGER PRIMARY KEY,
   "Aggregate_id" INTEGER
 );

 DROP TABLE IF EXISTS "public"."Run" CASCADE;
 CREATE TABLE "public"."Run" (
   "Run_id" INTEGER PRIMARY KEY,
   "RunFunction_id" INTEGER,
   "executeTime" VARCHAR(100),
   "createDateTime" VARCHAR(100),
   "authorityGUID" VARCHAR(100)
 );

 DROP TABLE IF EXISTS "public"."RunFunction" CASCADE;
 CREATE TABLE "public"."RunFunction" (
   "RunFunction_id" INTEGER PRIMARY KEY,
   "Function_id" INTEGER,
   "executeTime" VARCHAR(100),
   "createDateTime" VARCHAR(100),
   "authorityGUID" VARCHAR(100)
 );

 DROP TABLE IF EXISTS "public"."SourceConfig" CASCADE;
 CREATE TABLE "public"."SourceConfig" (
   "SourceConfig_id" INTEGER PRIMARY KEY
 );
 ALTER TABLE "public"."Deployment" ADD CONSTRAINT FK_Environment FOREIGN KEY (Environment_id) REFERENCES "Environment"(Environment_id);
 ALTER TABLE "public"."Deployment" ADD CONSTRAINT FK_Graph FOREIGN KEY (Graph_id) REFERENCES "Graph"(Graph_id);
 ALTER TABLE "public"."Deployment" ADD CONSTRAINT FK_Run FOREIGN KEY (Run_id) REFERENCES "Run"(Run_id);
 ALTER TABLE "public"."Deployment" ADD CONSTRAINT FK_SourceConfig FOREIGN KEY (SourceConfig_id) REFERENCES "SourceConfig"(SourceConfig_id);
 ALTER TABLE "public"."Function" ADD CONSTRAINT FK_InputSet FOREIGN KEY (InputSet_id) REFERENCES "InputSet"(InputSet_id);
 ALTER TABLE "public"."Function" ADD CONSTRAINT FK_OutputSet FOREIGN KEY (OutputSet_id) REFERENCES "OutputSet"(OutputSet_id);
 ALTER TABLE "public"."Graph" ADD CONSTRAINT FK_Function FOREIGN KEY (Function_id) REFERENCES "Function"(Function_id);
 ALTER TABLE "public"."OutputSet" ADD CONSTRAINT FK_Aggregate FOREIGN KEY (Aggregate_id) REFERENCES "Aggregate"(Aggregate_id);
 ALTER TABLE "public"."Run" ADD CONSTRAINT FK_RunFunction FOREIGN KEY (RunFunction_id) REFERENCES "RunFunction"(RunFunction_id);
 ALTER TABLE "public"."RunFunction" ADD CONSTRAINT FK_Function FOREIGN KEY (Function_id) REFERENCES "Function"(Function_id);
