import java.io.File
import java.lang.System.exit

import mythago.plato.{LogHelper, Session}
import mythago.plato.meta.MetaHOCON

object Main extends App with LogHelper {
  val usage = """
    Usage: plato [--config] filename [--projection] projName
  """

  println("PLATO BOOT SEQUENCE")
  if (args.length == 0) println(usage)
  val arglist = args.toList
  type OptionMap = Map[String, Any]
  val cmdArgs = getOption(Map(),arglist)
  println(cmdArgs)

  log.info("LOGGING BOOT SEQUENCE File = " + cmdArgs("config") )
  log.debug("ROOT FOLDER " + System.getProperty("user.dir"))

  val myConfigFile = cmdArgs("config").toString

  val checkSession =  Session.initialise(myConfigFile)
  if (checkSession.isEmpty) {
    print("Session initialisation failed")
  }
  val session = checkSession.get
  log.debug(session.dump())

  cmdArgs.filter(a1 => a1._1 == "projection").foreach(p => session.execProjection(p._2.toString))
  cmdArgs.filter(a1 => a1._1 == "transform").foreach(tc => session.execTransform(tc._2.toString))
  cmdArgs.filter(a1 => a1._1 == "ideology").foreach(p => session.execIdeologyProjection(p._2.toString))


  def getOption(map : OptionMap, list: List[String]) : OptionMap = {
    def isSwitch(s : String) = (s(0) == '-')
    list match {
      case Nil => map
      case "--config" :: value :: tail =>
        getOption(map ++ Map("config" -> value.toString), tail)
      case "--projection" :: value :: tail =>
        getOption(map ++ Map("projection" -> value.toString), tail)
      case "--transform" :: value :: tail =>
        getOption(map ++ Map("transform" -> value.toString), tail)
      case "--ideology" :: value :: tail =>
        getOption(map ++ Map("ideology" -> value.toString), tail)
      case option :: tail => println("Unknown option "+option)
        exit(1)
        null
    }
  }
}
