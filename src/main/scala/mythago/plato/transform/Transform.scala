package mythago.plato.transform

import mythago.plato.LogHelper
import mythago.plato.elements.{Arc, Path}
import mythago.plato.encoding.EncodingDefinition
import mythago.plato.encoding.projection.Projection
import mythago.plato.transform.TraversalHelper._
import scalax.collection.Graph

class Transform(val srcProjection: Projection,
                val destProjection: Projection,
                val srcPaths: Seq[PathTransform],
                val transformDefinition: TransformDefinition) extends LogHelper
{
  def getDestSchemaName(label:String):Option[String] = {
    getSchema(label, destProjection.p.outputEncodings )
  }

  def getSrcSchemaName(label:String):Option[String] = {
    getSchema(label, srcProjection.p.outputEncodings )
  }

  def getSchema(label:String, eds:Set[EncodingDefinition]): Option[String]={
    eds.find(pe => pe.encodingType == label).map(_.schema)
  }
}

object Transform
  extends LogHelper
{
  // given src projection find every path to a node through the source graph
  def getPathsOverNodes(src:Projection,
                        dest:Projection): Seq[Path] = {
    val sgraph = src.g.getOrElse(Graph.empty)

    // get all Nodes mapping from base project to ideology projection
    val destProjParents = dest.lineage.getParents()
    val allSourceNodes = destProjParents.flatMap(n => src.lineage.getChildrenTraverser(n))

    log.debug(" >>>> getPathsOverNodes -------------------------------------");
    log.debug(" =====+====== Source Graph \n "+src.dumpGraph())
    log.debug(" ============ Source Graph lineage \n "+src.lineage.family.mkString("\n"))
    log.debug(" ============ Dest Graph \n "+dest.dumpGraph())
    log.debug(" ============ Dest Graph lineage \n "+dest.lineage.family.mkString("\n"))

    val thePaths = allSourceNodes.flatMap(n => allPathsFrom(n,sgraph)).toList
    log.debug(" >>>> Paths to Nodes -------------------------------------");
    log.debug(dumpPaths(thePaths))

    thePaths.flatMap( t => t._2.map(s => Path(t._1,s,src.lineage) ) )
  }

  // purpose is to collect
  def mapPathsToDestination(src:Projection, dest:Projection, paths:Seq[Path]): Seq[PathTransform] = {
    val dlineage = dest.lineage
    val slineage = src.lineage

    // ( Set(<parentProjectionNode>) , path(sNode,Arcs) )
    val sParentsFromPaths = paths.map(p => (slineage.getParentOf(p.start),p) ).toSet

    val destChildFromSrcPath =
      sParentsFromPaths.flatten( b => b._1.map(pn => (pn, dlineage, dlineage.getChildren(pn), b._2 )) )

    destChildFromSrcPath.flatten(b => b._3.map(n => PathTransform(b._1,n._1,b._2,n._2,b._4)) ).toSeq
  }

  def exec(src:Projection, dest:Projection, transEnc:TransformDefinition): Option[Transform] =
  {
    if (src != null && dest != null) {
      val rawPaths = getPathsOverNodes(src, dest)
      val srcPaths = mapPathsToDestination(src, dest,rawPaths)
      Some(this.apply(src, dest, srcPaths, transEnc))
    } else
      None
  }

  def apply(srcP:Projection,
            destP:Projection,
            srcPaths: Seq[PathTransform],
            transDef:TransformDefinition): Transform =
  {
    new Transform(srcP, destP, srcPaths, transDef)
  }
}
