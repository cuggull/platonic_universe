package mythago.plato.transform

import mythago.plato.Platonics
import mythago.plato.elements.{Arc, Node}
import scalax.collection.Graph
import scalax.collection.edge.LkDiEdge

import scala.collection.Set
import scala.collection.immutable.SortedSet

object TraversalHelper {
  def getNodeByName(g: Graph[Node, LkDiEdge], name: String): Option[Node] = {
    g.nodes.toOuter.find(n => n.name == name)
  }

  def getNodes(g: Graph[Node, LkDiEdge]): SortedSet[Node] =
    SortedSet[Node]() ++ g.nodes.map(n => n.toOuter).toSet;

  def getInnerNodes(g: Graph[Node, LkDiEdge]) =
    g.nodes.map(x2 => x2.self).map(x3 => x3.edges)

  def getAdjacentNodes(g: Graph[Node, LkDiEdge], n: Node): SortedSet[Node] = SortedSet[Node]() ++ g.get(n).edges.map(y => y.toOuter.to).toSet

  def getAdjacentArcs(g: Graph[Node, LkDiEdge], n: Node): SortedSet[Arc] = {
    SortedSet[Arc]() ++ g.get(n).edges.map(y => y.toOuter.label.asInstanceOf[Arc]).toSet.filter(a1 => a1.end.name != n.name)
  }

  def getAdjacentArcsN2(g: Graph[Node, LkDiEdge], n: Node): SortedSet[Arc] = {
    SortedSet[Arc]() ++ g.get(n).edges.filter(a1 => a1.target != n)
      .map(y => y.toOuter.label.asInstanceOf[Arc]).toSet
  }

  def getArcs(g: Graph[Node, LkDiEdge]): SortedSet[Arc] = {
    SortedSet[Arc]() ++ g.edges.map(e => e.toOuter.label.asInstanceOf[Arc]).toSet
  }

  def allPaths( g: Graph[Node, LkDiEdge]): List[(Node, List[Seq[Arc]])]
  = g.nodes.flatMap(n => allPathsFrom(n.toOuter,g) ).toList

  def allPathsFrom(inNode:Node, g: Graph[Node, LkDiEdge]): List[(Node, List[Seq[Arc]])] = {
    def destNode(n:g.NodeT, e:g.EdgeT):g.NodeT = if (e._2 == n) e._1 else e._2
    def outerEdge(e:g.EdgeT) = e.toOuter.label.asInstanceOf[Arc]
    def selfAdd(n:g.NodeT,p:List[Seq[Arc]] ):List[Seq[Arc]] =
      if (n.isLeaf) List[Seq[Arc]](Seq(Arc(inNode,Node(Platonics.ROOTNODE)))) ++ p
      else p
    def Paths(n: g.NodeT, tracking: Set[String]): List[Seq[Arc]] = {
      n.edges
        .flatMap(e => {
          val n1 = destNode(n,e)
          val lp = n1.toOuter.name
          if (tracking.contains(lp))
            List[Seq[Arc]] (Seq[Arc]())
          else
            Paths(n1, tracking ++ Set(n.toOuter.name)).map(p => Seq(outerEdge(e)) ++ p)
        }).toList
    }
    val foundNode = g.find(inNode)
    if (foundNode.isEmpty)
      List[(Node, List[Seq[Arc]])]()
    else
      List((inNode, selfAdd(foundNode.get, Paths(foundNode.get,Set()) )))
  }

  def dumpPaths(ps:List[(Node, List[Seq[Arc]])]) : String = {
    ps.map(a => ">>>>" + a._1 + "\n    " + a._2.map(sa => sa.foldLeft("\n"){(s,a) => s+"("+a.startIdentityAlias.getName()+","+a.endIdentityAlias.getName()+") " } ) ).mkString("\n")
  }

}
