package mythago.plato.transform

import mythago.plato.encoding.EncodingDefinition

class TransformDefinition(val name:String, val src:String, val dest:String, val outputEncodings: Set[EncodingDefinition] )
{

}

object TransformDefinition {
  def apply(name:String, src:String, dest:String, outputEncodings: Set[EncodingDefinition]):TransformDefinition =
  {
    new TransformDefinition(name,src,dest,outputEncodings)
  }
}