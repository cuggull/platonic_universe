package mythago.plato.transform

import mythago.plato.elements.{Arc, Node, Path}
import mythago.plato.encoding.projection.LineageType.lineageType
import mythago.plato.encoding.projection.ProjectionLineage

// PathTransform => sParentNode, DestChildNode, DestChildNodeLineageType, fromSourcePath
class PathTransform(val parent:Node,
                    val dest:Node,
                    val destLineage:ProjectionLineage,
                    val destLineageType:lineageType,
                    val sources:Set[Path])
{
  def getAllArcs():Set[Arc] = {
    sources.flatMap(p => p.arcs)
  }

  def dump():String = {
    "============================== Destination Details type='"+destLineageType+"' parent= "+parent+" :: " + dest + " ::\n" +
      sources.map(p => p.dump()).mkString("\n")
  }
}

object PathTransform {
  def apply (pn:Node, n:Node,
             destLineage:ProjectionLineage,
             lt:lineageType,
             paths:Set[Path]):PathTransform = {
    new PathTransform(pn,n,destLineage,lt,paths)
  }

  def apply (pn:Node, n:Node,
             destLineage:ProjectionLineage,
             lt:lineageType,
             path:Path):PathTransform = {
    new PathTransform(pn,n,destLineage,lt,Set(path))
  }
}