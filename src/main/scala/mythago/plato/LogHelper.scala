package mythago.plato
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

trait LogHelper {
    val loggerName: String = this.getClass.getName
    lazy val log = LogManager.getLogger(loggerName)
}
