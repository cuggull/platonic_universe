package mythago.plato.meta

import java.io.File
import java.util

import com.typesafe.config._
import mythago.plato.LogHelper
import mythago.plato.Platonics._
import mythago.plato.elements.{Arc, Attribute, Identity, Node, Universe}
import mythago.plato.encoding.EncodingDefinition
import mythago.plato.encoding.projection.{ProjectionRule, ProjectionRuleSet}
import mythago.plato.transform.TransformDefinition

import scala.collection.immutable.{HashMap, HashSet}
import scala.jdk.CollectionConverters._

class MetaHOCON(config: Config) extends Meta  with LogHelper {
  def this(file: File) {
    this(ConfigFactory.parseFile(file))
  }

  def this(content: String) {
    this(ConfigFactory.parseString(content))
  }

  /*

  identity ::= pathName + name
  pathName ::= "global." | <universeName>

  global : {
    attributes_lists : {
      al1 : [ a1, a2, a3, a4]
      al2 : []
    }
    nodes : [ n1,n2,n3,n4,n5 ]
  }

  PlatonicUniverses : {
      <universeName> : {
           attributes_lists [
                trade_bundle1 : [
                  {name:"exchange",type:"string",constraint:"not null",security:"0",tags:""},
                  {name:"zone",type:"string",constraint:"not null",security:"0",tags:""},
              ]
          ]
          nodes : [n1, n2, n3,
            {
              n4 : {
                identity: "xxx"
                attributes : [
                    trade_bundle1,
                    trade_bundle2,
                    {name:"exchange",type:"string",constraint:"not null",security:"0",tags:""}
                ]
              }
            }
          ]
          arcs : [ [n1,n2], [n1,n3], [n1,n4] ]
      }
    }
 */

  // ok fuck trying to play nice with java based config
  // unwrap to the raw values

  override def getUniverses(): Map[String, Universe] = {

    val co = getConfigSection(config,UNIVERSES)
    if (!co.isDefined) return Map[String,Universe]()

    val unis = co.get.root().unwrapped().keySet().asScala
      .map(uk  => getUniverse(uk,co.get.getConfig(uk)))

    log.trace(unis)

    // return the set as a map of universe name to universe
    Map() ++ unis.map(i => i.name -> i)
  }

  def getUniverse(uniName: String, co : Config) : Universe = {
    log.debug("------------ Entered GetUniverse "+uniName+ "---------------")

    // could inject look ups from a reference Universe for
    // attributeLists (classes without identity)
    // and nodes

    val attribs = getUnwrappedConfig(co,ATTRIBUTE_LISTS)
                            .flatMap(al => buildAttribBundles(al._1, al._2))

    val nodes  = getUnwrappedConfig(co,NODES)
        .flatMap(n => buildNodes(uniName,attribs,n._2) )

    val nodesAndArcNodes = getUnwrappedConfig(co,ARCS)
      .flatMap(n => buildNodesFromArcs(uniName,attribs,nodes,n._2) )

    val arcs = getUnwrappedConfig(co,ARCS)
      .flatMap(n => buildArcs(nodesAndArcNodes,n._2) )

    new Universe(uniName,attribs,nodes,arcs.toSet)
  }

  def buildAttribBundles(key:String,attribs:Any):Map[String,Set[Attribute]]  = {
    log.debug("Build bundle "+key)
    val attribSets  = attribs match {
      case attribList : java.util.ArrayList[Any] => {
        attribList.asScala.map(a => a match {
          case nodeStructure : java.util.HashMap[String,Object] => {
            val a = Attribute(nodeStructure.asScala)
            log.debug(" "+a.dump("    "))
            a
          }
          case unknown: Any => {
            log.error("  Attribute Unexpected Structure will be filtered out"+unknown.getClass)
            null
          }
        }).toSet.filter(a => a != null)
      }
      case unknown: Any => {
        log.debug(unknown.getClass)
        Set[Attribute]()
      }
    }
    Map[String, Set[Attribute]](key -> attribSets)
  }

  def buildNodes(uniName:String,attributeLists:Map[String, Set[Attribute]],
                 nodes:Any): Map[String,Node] = {
    log.debug("Entered buildNodes =")
    val nodesset = nodes match {
      case nodelist : Set[Any] => {
        nodelist.flatMap(n => n match {
            case name: String => {
              log.debug("    >>" + name)
              buildNode(uniName,attributeLists,name)
            }
            case nodeStructure: java.util.HashMap[String, Any] => {
              log.debug(nodeStructure.keySet())
              buildNode(uniName,attributeLists,nodeStructure.asScala.toMap)
            }
          }
        )
      }
      case value: Any => {
        log.error("Unexpected, looking for nodelist - got a " + value.getClass)
        Set[Node]()
      }
    }
    Map() ++ nodesset.map(i => i.name -> i)
  }

  def buildNode(uniName:String,attributeLists:Map[String, Set[Attribute]], nodeName:String ): Set[Node] ={
    log.trace("Entered buildNode ("+nodeName+")")
    Set(Node(nodeName,uniName))
  }

  def buildNode(uniName:String,attributeLists:Map[String, Set[Attribute]],
                nodeStructure:Map[String,Any]): Set[Node] ={
    log.trace("Entered buildNode structured (~)")
    //
    // node : [a,b,c {a,b,c}, x, y, z]
    //
    // unpack node structure + attributes

    val nodeConf = nodeStructure.toSeq.head
    log.trace (" Building Node --> " + nodeConf._1)

    val attributes = nodeConf._2 match {
      case attribs : java.util.ArrayList[Any] => {
        log.trace(" its an array of attributes")
        attribs.asScala.flatMap(a => a match {
          case attribBundleName : String => {
            attributeLists.getOrElse(attribBundleName,
              Set[Attribute]( Attribute(attribBundleName,REFERENCE)))
          }
          case attribtConfig : java.util.HashMap[String,Object] =>
            Set[Attribute]( Attribute(attribtConfig.asScala))
        })
      }
      case o:Object => {log.error("  well i didnt expect that "+o.getClass)
        Set[Attribute]()}
    }

    // locate any identity definition
    val id_attrib = attributes.find(a => a.valueType == IDENTITY)
    // otherwise use attributes except identity as its special
    val other_attribs = attributes.filter(a => a.valueType != IDENTITY).toList

    if (id_attrib.isDefined)
      Set(Node(nodeConf._1, uniName, Identity(id_attrib.get.name), other_attribs))
    else
      Set(Node(nodeConf._1,uniName,other_attribs))
  }

  private def buildNodesFromArcs(uniName:String,attributeLists:Map[String, Set[Attribute]],
                                 nodes: Map[String, Node], arcConf:Any): Map[String, Node] = {
    log.debug("Build buildNodesFromArcs ")
    val fullNodes  = arcConf match {
      case node_tuples : Set[Any] => {
        node_tuples.flatMap(a => a match {
          case node_tuple_array :  java.util.ArrayList[Object] => {
            log.debug(node_tuple_array.toString)
            if (node_tuple_array.size() == 2) {
              val n1 = if (!nodes.contains(node_tuple_array.get(0).toString))
                buildNode(uniName,attributeLists,node_tuple_array.get(0).toString)
              else Set[Node]()
              val n2 = if (!nodes.contains(node_tuple_array.get(1).toString))
                buildNode(uniName,attributeLists,node_tuple_array.get(1).toString)
              else Set[Node]()
              n1 ++ n2
            } else
              null
          }
          case unknown: Any => {
            log.error(unknown.getClass)
            null
          }
        }).filter(a => a != null)
      }
      case unknown: Any => {
        log.error(unknown.getClass)
        null
      }
    }
    nodes ++ fullNodes.filter( a => a != null).map(i => i.name -> i)
  }

  private def buildArcs(nodes: Map[String, Node], arcConf:Any): Set[Arc] = {
    log.debug("Build Arcs ")
    val arcs  = arcConf match {
      case node_tuples : Set[Any] => {
        node_tuples.map(a => a match {
          case node_tuple_array :  java.util.ArrayList[Object] => {
            log.debug(node_tuple_array.toString)
            if (node_tuple_array.size() == 2) {
              val n1 = nodes.get(node_tuple_array.get(0).toString)
              val n2 = nodes.get(node_tuple_array.get(1).toString)
              if (n1.isDefined && n2.isDefined)
                Arc(n1.get, n2.get)
              else
                null
            } else
              null
          }
          case unknown: Any => {
            log.error(unknown.getClass)
            null
          }
        }).toSet.filter(a => a != null)
      }
      case unknown: Any => {
        log.error(unknown.getClass)
        null
      }
    }
    arcs.filter( a => a != null)
  }

  override def getGlobals(): Universe = {
    // load global Nodes, load global attributes
    val co = getConfigSection(config,GLOBALS)
    if (!co.isDefined)
      return new Universe(GLOBALS)
     getUniverse( GLOBALS,co.get)
  }

  override def getProjectionRuleSets(): Map[String, ProjectionRuleSet] ={
    val co = getConfigSection(config,PROJECTIONSETS)

    if (!co.isDefined) return Map[String,ProjectionRuleSet]()

    val projs = co.get.root().unwrapped().keySet().asScala
      .map(uk  => getProjectionRuleSet(uk,co.get.getConfig(uk))).toSet

    Map() ++ projs.map(i => i.name -> i)
  }

  def findString(config:Config,key:String):String = {
    if (config.hasPath(key)) return config.getString(key) else ""
  }

  def getProjectionRuleSet(name: String, co : Config) : ProjectionRuleSet = {
    log.debug("------ Entered getProjectionRuleSet "+name+ "---------")
    // get source universe
    val rsUniverse =  findString(co,UNIVERSE)
    // get 'from' start node
    val rsFrom = findString(co,FROM)
    // get encodings
    val rsEncodings = co.getList(ENCODINGS).unwrapped().asScala
      .map(e => buildEncodingDefinition(e)).toSet
    // get rulesets
    val rsRulesetConf = co.getList(RULESET)
    val rsIdeology = findString(co,APPLY_IDEOLOGY)
    val rsApplyIdeology = findString(co,APPLY_IDEOLOGY)

    log.debug( rsUniverse + "::" + rsFrom + "::" + rsEncodings)
    val rsRuleSet = rsRulesetConf.unwrapped().asScala.map(r => buildProjectionRule(r)).toSet
    ProjectionRuleSet(name,rsEncodings,rsRuleSet,rsUniverse,rsFrom,"",rsApplyIdeology,rsIdeology)
  }

  def buildProjectionRule(ruleConf:AnyRef): ProjectionRule ={
    ruleConf match {
      case rule : util.HashMap[String,ConfigValue] => {
          log.trace ("Nailed it  "+rule.getClass)
          val rm = rule.asScala
          // intentionally going through hoops to isolate HOCON Config references within just METAHOCON
          new ProjectionRule(Map()++ rm.keys.map((k => k -> rm.apply(k).toString)))
        }
      case value: AnyRef => {
        log.error ("Ooops its a "+value.getClass)
        null
      }
    }
  }

  def buildEncodingDefinition(config: AnyRef): EncodingDefinition = {
    config match {
      case rule : util.HashMap[String,ConfigValue] => {
        log.trace ("Nailed it  "+rule.getClass)
        val rm = rule.asScala
        // intentionally going through hoops to isolate HOCON Config references within just METAHOCON
        new EncodingDefinition(Map()++ rm.keys.map((k => k -> rm.apply(k).toString)))
      }
      case value: AnyRef => {
        log.error ("Ooops its a "+value.getClass)
        null
      }
    }
  }

  override def getTransforms(): Map[String, TransformDefinition] = {
    val co = getConfigSection(config,TRANSFORMS)

    if (!co.isDefined) return Map[String,TransformDefinition]()

    val ts = co.get.root().unwrapped().keySet().asScala
      .map(uk => getTransform(uk,co.get.getConfig(uk))).toSet


    Map() ++ ts.map(i => i.name -> i)
  }

  def getTransform(name: String, co : Config) : TransformDefinition = {
    log.debug("------ Entered getTransform "+name+ "---------")
    // get source universe
    val tsource =  findString(co,SOURCE)
    // get 'from' start node
    val tdest = findString(co,DESTINATION)
    // get encodings
    val tEncodings = co.getList(ENCODINGS).unwrapped().asScala
      .map(e => buildEncodingDefinition(e)).toSet
    log.debug( tsource + "::" + tdest + "::" + tEncodings)

    TransformDefinition(name,tsource,tdest,tEncodings)
  }

  override def dump(): String = ???

  //  task name mapping to compiled task - TBD when we compileConfig tasks

  private def getConfigSection(inconf:Config, label: String): Option[_<:Config] = {
    log.trace("Entered getConfigSection")

    var co: Option[_<:Config] = Option.empty[Config]

    if (inconf.hasPath(label) == true)
      co = Some(inconf.getConfig(label))
    else
      log.warn("Config does not contain a valid definition for::" + label)
    co
  }

  private def getUnwrappedConfig(inconf:Config, label: String):  Map[String,Any]  = {
    log.trace("Entered getUnwrappedConfig")

    val x = inconf.root().unwrapped().asScala.get(label)

    if (!x.isDefined) return Map[String,Any]()

    x.get match {
      case input: util.ArrayList[ConfigValue] => {
        log.debug("getUnwrappedConfig:: appears to be any array of config")
        HashMap[String, Any](label -> input.asScala.toSet)
      }
      case input: util.HashMap[String, ConfigValue] => {
        log.debug("getUnwrappedConfig:: dude appears to be a Hashmap of configvalue!!")
        input.asScala.toMap
      }
      case input: Any => {
        log.error("getUnwrappedConfig:: sorry dude it aint any of the above " + input.getClass)
        Map[String,Any]()
      }
    }
  }

}
