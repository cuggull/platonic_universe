package mythago.plato.meta
import mythago.plato.elements._
import mythago.plato.encoding.projection.ProjectionRuleSet
import mythago.plato.transform.TransformDefinition

abstract class Meta {

  def getGlobals: Universe

  def getUniverses: Map[String, Universe]

  def getProjectionRuleSets: Map[String, ProjectionRuleSet]

  def getTransforms: Map[String, TransformDefinition]

  def dump(): String
}
