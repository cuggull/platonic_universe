package mythago.plato.encoding.cmd

import mythago.plato.Platonics._
import mythago.plato.LogHelper
import mythago.plato.elements.Arc
import mythago.plato.encoding.{EncodeTools, Encoding, EncodingDefinition, EncodingTransform}
//import mythago.plato.encoding.schema.Encoding_SQL_Postgres_Raw
import mythago.plato.transform.Transform

object Cmd_SQL_Postgres extends EncodeTools with LogHelper{

  val QUERY_SUBJECT = "FROM"
  val QUERY_RESULT = "SELECT"
  val QUERY_FILTER = "WHERE"

  /*
    CREATE ( dest:PATH): NODE

    COPY ( src:PATH, dest:PATH ) = WRITE( CREATE(dest) , TRAVERSE (src) )

    TRAVERSE( keys, src:PATH ) : NODES =  {
      if path.head == null -> return
      // keys = READ( PATH.head.key )
      TRAVERSE( TRAVERSE(PATH.tail) ,   QUERY( PATH.head.key ) )
    }
    (query ( predicate ) -> result)
    query <=  src.query()

   */

  def queryJoinTable(): String = {
    ""
  }

  def queryKeys(c:Command, p: Seq[Arc], a:Option[Arc], result:String ):String =
  {
    log.debug("entered queryKeys " + c.dump)
    // this should never trigger as length should
    // always be greater than 0 if it enters here
    if ( a.isEmpty ) return ""
    val theArc = a.get

    if (false && theArc.multiplicity == MANY
      && theArc.direction == BIDIRECTIONAL) {
      /*
        work out the next arc - some arcs create a many to many join table
            A [ <-> B ] for (Bidirectional, many) -> C for (right, single)
            A [ <-  A_B -> B ]
            A (a.id) <- (a.id) A_B (b.id) -> (b.id) B (c.id) -> c.d C

            bidirectional,join:  A (a.id) <- (a.id) A_B (b.id) -> (b.id) B
            left :  A (a.id) <- (a.id) B : Se ? Fr A Wh a.id in ( )
            right :  A (b.id) -> (b.id) B

      */
      //  val intermediatePath = queryKeys()
      val buf = s"""SELECT "${theArc.startIdentityAlias.getName()}" FROM ${} WHERE ${theArc.endIdentityAlias.getName()} """

      if (true)
        buf + s"""WHERE "${theArc.endIdentityAlias.getName()}" IN """
      else
        buf
    }
    else
    {
      val buf = s"""SELECT "${theArc.endIdentityAlias.getName()}" FROM "${theArc.start.name}"  """
      if (p.length > 0) {
        buf + s"""WHERE "${theArc.start.id.getName()}" IN (""" +
          queryKeys(c, p.tail, p.headOption, result) + ")"
      } else
        buf
    }
  }

  def querySet(c:Command, p: Seq[Arc], result:String ):String = {
    val baseQuery =  s"""SELECT * FROM "${c.destNode.name}" """
    if (p.length > 0) {
      val retQuery = queryKeys(c,p.tail, p.headOption ,result)
      if (retQuery.length > 0)
        s""" $baseQuery WHERE "${c.destNode.id.getName()}" IN ($retQuery)"""
      else
        baseQuery
    } else {
      baseQuery
    }
  }

  def buildQuery(t:Transform, c:Command) : String = {
    val srcId = ""
    val srcNode = ""
    val revSeq = c.srcPath.reverse

    log.debug(revSeq)

    if (c.srcPath.length > 0)
      // s"""$QUERY_START $srcId $QUERY_SUBJECT $srcNode  """
      querySet(c,revSeq,"")
    else
     querySet(c,Seq(),"")
  }

  def encode(t:Transform, cmds:Seq[Command]) : String = {
    log.debug("----------------- Entered Transform ------------------ ")
    //
    //  Processing Command Sequence
    cmds.map(c => c.cmd match {
      case Command.CREATE => {
        "-- Ignorning CREATE for SQL " + c.dump
      }
      case Command.COPY => {
        buildQuery(t,c)
      }
      case cmd:Any => {
        log.error("Unknown Command " + cmd)
        ""
      }
    }).mkString("\n")
  }

  def apply(e:EncodingDefinition, t:Transform): Encoding = {
    EncodingTransform(e, t, encode(t,Command.build(e,t)))
  }

}
