package mythago.plato.encoding.cmd

import mythago.plato.LogHelper
import mythago.plato.Platonics.{BIDIRECTIONAL, POSTGRES}
import mythago.plato.elements.{AnIdentity, Arc, Identity, Node, Path}
import mythago.plato.encoding.projection.{LineageType, Projection}
import mythago.plato.encoding.projection.LineageType.lineageType
import mythago.plato.encoding.{EncodeTools, Encoding, EncodingDefinition, EncodingTransform}
import mythago.plato.transform.PathTransform
import mythago.plato.transform.Transform

object Cmd_SQL_Postgres_opt2 extends EncodeTools with LogHelper {

  val QUERY_SUBJECT = "FROM"
  val QUERY_RESULT = "SELECT"
  val QUERY_FILTER = "WHERE"

  def innerJoin(p: Path, parentNode:Node, s: Seq[Arc], a: Option[Arc], schemas:(String,String)): String = {
    if (a.isEmpty) return ""
    val theArc = a.get

    log.debug(s"     Arc(${parentNode.name}) = ${theArc.dump()}")

    val startNode = if (theArc.start == parentNode) theArc.start else theArc.end
    val endNode = if (theArc.start == parentNode) theArc.end else theArc.start
    val startIdentity = theArc.startIdentityAlias
    val endIdentity =  theArc.endIdentityAlias

    log.debug(s"     (${startNode.name}.${startIdentity.getName()}, ${endNode.name}.${endIdentity.getName()}")

    val buf = s"""INNER JOIN ${dot(schemas._1,endNode.name)} ON ${dot(schemas._1,endNode.name,endIdentity.getName())} = ${dot(schemas._1,startNode.name,endIdentity.getName())}  """

    buf + innerJoin(p, endNode, s.drop(1), s.headOption, schemas)
  }

  def getAttributes(n:Node, schema:String): String = n.attributes.map(a => s"""${dot(schema,n.name,a.name)}""").mkString(",")
  def getIdentity(n:Node, schema:String): String = s"""${dot(schema,n.name,n.getId())}"""
  def getAttributesDS(n:Node): String = n.attributes.map(a => s"""${quote(a.name)}""").mkString(",")
  def getIdentityDS(n:Node): String = s"""${quote(n.getId())}"""
  def getLinks(n:Node, masterLinks: Seq[(Node, AnIdentity)], p:Path, schemas:(String,String)): String = {
    def getIdentityAlias(n: Node, a: Arc): String = {
      if (n == a.start)
        a.startIdentityAlias.getName()
      else
        a.endIdentityAlias.getName()
    }
    val y = masterLinks.map(nd =>  (nd._1, p.arcs.find(a =>a.contains(nd._1)) ))
    y.map(t1 =>
      if (t1._2.isDefined)
        s""" ${dot(schemas._1,t1._1.name,getIdentityAlias(t1._1, t1._2.get))} AS "${t1._1.name}_${getIdentityAlias(t1._1, t1._2.get)}" """
      else
        s"""null::integer AS "${t1._1.name}_${masterLinks.find(_._1 == t1._1).get._2.getName()}" """
    ).mkString(",")
  }

  /*
  def getLinksDS(n:Node, masterLinks: Seq[(Node, Identity)], schemas:(String,String)): String = {
    def getIdentityAlias(n: Node, a: Arc): String = {
      if (n == a.start)
        a.startIdentityAlias.name
      else
        a.endIdentityAlias.name
    }
    val y = masterLinks.map(nd =>  (nd._1, p.arcs.find(a =>a.contains(nd._1)) ))
    y.map(t1 =>
      if (t1._2.isDefined)
        s""" ${dot(schemas._1,t1._1.name,getIdentityAlias(t1._1, t1._2.get))} AS "${t1._1.name}_${getIdentityAlias(t1._1, t1._2.get)}" """
      else
        s"""null::integer AS "${t1._1.name}_${masterLinks.find(_._1 == t1._1).get._2.name}" """
    ).mkString(",")
  }
*/

  def getKeys(n:Node, p:Projection):String = {
    val arcs = p.getAdjacentArcs(n)
    arcs.filter(a1=> a1.direction != BIDIRECTIONAL).map(a => quote(n.name+"_"+a.endIdentityAlias.getName())).mkString(",")
  }

  def attributeList(n:Node, masterLinks: Seq[(Node, AnIdentity)], p:Path, schemas:(String,String), destLT:lineageType) : String = {

    // destination type of node determines attributes needed
    destLT match {
      case LineageType.full => getIdentity(n,schemas._1) + separator(getAttributes(n,schemas._1)) + separator(getLinks(n,masterLinks,p,schemas))
      case LineageType.arc =>  getLinks(n,masterLinks,p,schemas)
      case LineageType.attribute => getIdentity(n,schemas._1) + separator(getAttributes(n,schemas._1))
      case LineageType.identity => getIdentity(n,schemas._1) + separator(getAttributes(n,schemas._1))
      case LineageType.link => getIdentity(n,schemas._1) + separator(getLinks(n,masterLinks,p,schemas))
    }
  }
/*
  def attributeListDS(n:Node, masterLinks: Seq[(Node, Identity)], schemas:(String,String), destLT:lineageType) : String = {

    // destination type of node determines attributes needed
    destLT match {
      case LineageType.full => getIdentity(n,schemas._1) + separator(getAttributes(n,schemas._1)) + separator(getLinks(n,masterLinks,p,schemas))
      case LineageType.arc =>  getLinks(n,masterLinks,p,schemas)
      case LineageType.attribute => getIdentity(n,schemas._1) + separator(getAttributes(n,schemas._1))
      case LineageType.identity => getIdentity(n,schemas._1) + separator(getAttributes(n,schemas._1))
      case LineageType.link => getIdentity(n,schemas._1) + separator(getLinks(n,masterLinks,p,schemas))
    }
  }
*/
  def queryPath(p: Path, s: Seq[Arc], masterLinks: Seq[(Node, AnIdentity)], schemas:(String,String),destLT:lineageType):String = {
    log.debug(s"======= Processing Arc for ${p.start.name} =========")
    s"""SELECT ${attributeList(p.start,masterLinks,p,schemas,destLT)} FROM ${dot(schemas._1,p.start.name)} ${innerJoin(p, p.start, s.tail, s.headOption, schemas)}"""
  }

  def queryPathEntry(p: Path, masterLinks: Seq[(Node, AnIdentity)], schemas:(String,String), destLT:lineageType): String = {
    val revSeq = p.arcs
    if (p.arcs.nonEmpty) queryPath(p, revSeq, masterLinks,schemas,destLT)
    else {
      log.debug(" -----> Empty Arc List <--------")
      queryPath(p, Seq(), masterLinks,schemas,destLT)
    }
  }

  def buildQueryUnions(n:Node, schemas:(String,String), pts:Seq[PathTransform], destProjection:Projection) : String = {
    def getAdjacentArcs: Seq[(Node, AnIdentity)] = {
      val identityNodes = pts.flatMap(pt => pt.sources.map(p => (p.start,p.start.id)))
      val adjacentArcs = pts.flatMap(pt => pt.sources.map(p => p.arcs.headOption))
        .flatten.map(a => if(a.start == n) (a.end, a.endIdentityAlias)
                          else (a.start, a.startIdentityAlias ))
      (identityNodes ++ adjacentArcs).distinct.sortBy(t => t._1.name)
    }

    val dataSetName = s"Plato_Unions_${n.name}"
    val masterLinks = getAdjacentArcs
    val entityElements = // attributeList(n,masterLinks,p,schemas,destLT)
      s"${getIdentityDS(n)} ${separator(getAttributesDS(n))} ${separator(getKeys(n,destProjection))} "

    val baseQuery = s"SELECT ${entityElements} FROM (\n"
    val joinQuery = s"\n Union \n"
    val tailQuery = s" ) ${dataSetName} ;\n"

    log.debug(s"AdjacentArcs(${n.name}) == "+getAdjacentArcs.mkString("\n"))
    val x = pts.flatten(pt => pt.sources.map(p => queryPathEntry(p,masterLinks,schemas,pt.destLineageType) ))
    log.debug(" ----------------------------------------------------")
    baseQuery + x.mkString(joinQuery) + tailQuery
  }

  def encode(t:Transform) : String = {
    log.debug("----------------- Entered SQL Encode Transform ------------------ ")
    val srcSchema = t.getSrcSchemaName(POSTGRES).getOrElse("public")
    val destSchema = t.getDestSchemaName(POSTGRES).getOrElse("public")
    val groupedDest = t.srcPaths.groupBy(pt => pt.dest)
    groupedDest.map(p => buildQueryUnions(p._1,(srcSchema,destSchema),p._2,t.destProjection)).mkString("\n")
  }

  def apply(e:EncodingDefinition, t:Transform): Encoding = {
    EncodingTransform(e, t, encode(t))
  }

}
