package mythago.plato.encoding.cmd

import mythago.plato.elements.{Arc, Node, Path}
import mythago.plato.encoding.EncodingDefinition
import mythago.plato.transform.Transform

case class Command(val cmd:String,
                   val srcNode:Node,
                   val srcPath:Seq[Arc],
                   val destNode:Node) {

  def dump() : String = {
    s" $cmd " + { cmd match {
      case Command.CREATE => s"  ${destNode.name}"
      case Command.COPY => s" ( ${srcNode.name}=>${destNode.name} ) :: ${srcPath.map(a => a.dump("")).mkString(",")}"
      case _ =>  " "
    } }
  }
}

object Command {
  val CREATE = "create"
  val COPY = "copy"

  def create(raw:Path): Command = {
    // each raw results in a CREATE & COPY
    this.create(raw.start,raw.arcs)
  }

  def copy(raw:Path): Command = {
    // each raw results in a CREATE & COPY

    this.copy(raw.arcs.last.end,raw.arcs, raw.start)
  }

  def create(node:Node, path:Seq[Arc]) : Command = {
    new Command(CREATE,null, Seq[Arc](),node)
  }

  def copy(srcNode:Node, srcPath:Seq[Arc], destNode:Node) : Command = {
    new Command(COPY,srcNode, srcPath,destNode)
  }

  def build(e:EncodingDefinition,
            t:Transform):Seq[Command] = {
    val sortedPaths = t.srcPaths.sortBy(_.sources.headOption match {
      case p:Option[Path] => p.get.start.toString
      case _ => ""
    })
    sortedPaths.flatten(p => {
      p.sources.map(p1 => Command.create(p1)) ++ p.sources.map(p2 => Command.copy(p2))
    })
  }

}