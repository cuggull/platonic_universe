package mythago.plato.encoding.cmd

import mythago.plato.LogHelper
import mythago.plato.elements.Path
import mythago.plato.encoding.{Encoding, EncodingDefinition, EncodingTransform}
import mythago.plato.encoding.projection.Projection
import mythago.plato.transform.{PathTransform, Transform, TransformDefinition}

object Cmd_DSL extends LogHelper {

  def apply( e:EncodingDefinition, t:Transform) : Encoding = {
    // Generate individual Commands
    log.debug("Building Command Sequence from Raw")
    val cmds = Command.build(e,t)
    EncodingTransform(e,t, dump(cmds))
  }

  def dump(cmds:Seq[Command]):String = {
    "\n"+cmds.map(c => s"${c.dump}").mkString("\n")
  }

}
