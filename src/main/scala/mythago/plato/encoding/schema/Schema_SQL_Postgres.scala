package mythago.plato.encoding.schema

import mythago.plato.LogHelper
import mythago.plato.Platonics._
import mythago.plato.elements.{Arc, Attribute, Node}
import mythago.plato.encoding.projection.Projection
import mythago.plato.encoding.{EncodeTools, Encoding, EncodingDefinition, EncodingProjection}
import scala.collection.immutable.SortedSet

object Schema_SQL_Postgres
    extends EncodeTools with LogHelper
{
    /*
          BNF

            NodeList =  Node [ |  Node ]
            Node = "Create table " <T> " begin " <IdentityList> <ArcList> <AttributeList> "end"
            BiDirectional_Link = " Create table " <T1>"_"<T2> " begin " <IdentityList>(t1,t2) "end"

            5ArcList ::= Node_Type [ | ArcList ]
            Node_Type ::= "Create Type " t1 "  AS ("

         */

    def encodingHeader(token:String, schema:String): String =
      s"""
        |DO $$
        |BEGIN
        |    IF NOT EXISTS(
        |        SELECT schema_name
        |          FROM information_schema.schemata
        |          WHERE schema_name = '${schema}'
        |      )
        |    THEN
        |      EXECUTE 'CREATE SCHEMA ${schema}';
        |    END IF;
        |END $$;
        """.stripMargin

    def encodingStart(token: String, name: String): String = ""
    def encodingEnd(toke: String) = ""

    def nodeStart(schema:String,
                  name: String): String = s"""DROP TABLE IF EXISTS "$schema"."$name" CASCADE;\n CREATE TABLE "$schema"."${name}" ("""

    // end of class
    def nodeEnd(): String = ");\n"

    def nodeTypeStart(name: String): String =  s"""CREATE TYPE "${name}" AS ("""

    def nodeTypeEnd(): String = ");"

    def keyFormat(token:String,key:String) = {
      s"""$token"$key" INTEGER"""
    }

    def key(token: String, thisNode: Node, arc: Arc) : String = {
      /*
      <key_name> integer,
      constraint fk_<nodeName>
      foreign key (<key_name>)
      REFERENCES students (<key_name>)
      */
      log.debug("key ="+arc.dump(">>"))
      arc.endIdentityAlias.getKeys.map(i => keyFormat(token,i.name)).mkString(",")
    }

    def keys(sc: EncodingProjection, token: String, d: Node): String = {
      val arcs = sc.p.getAdjacentArcs(d)
      arcs.filter(a1=> a1.direction != BIDIRECTIONAL).map(a => key(token,d,a)).mkString(",\n")
    }

    def primaryKey(key: String): String = keyFormat("",key) + " PRIMARY KEY"

    def convertType(inType:String, a:Attribute): String = {
      inType match {
        case STRING => "VARCHAR(100)"
        case INTEGER => "INT"
        case BOOLEAN => "BOOLEAN"
        case FLOAT => "REAL"
        case DATETIME => "DATETIME"
        case value: AnyRef => {
          log.error(s"Unknown attribute type '$value' converting to Varchar")
          "VARCHAR(99)"
        }
      }
    }

    def attribute(token: String, a: Attribute): String =
      line( token, 0 , quote(a.name) + " " + convertType(a.valueType,a) )

    def attributes(token: String, attributeList: List[Attribute]): String = { // name : { type : <type>, uuid: <uuid> }
      attributeList.map(a => attribute(token, a)).mkString(",\n")
    }

    // traverse through dag and generate schema
    def node(sc: EncodingProjection, n:Node, token: String): String =
    {
      lineN(token,0,nodeStart(sc.encode.schema,n.name) ) +
        lineN(token,2,primaryKey(n.getId())) +
        separator(keys(sc, indent(token,2), n))  +
        separator(attributes(indent(token,2),n.attributes) )+
        lineN(token,0,nodeEnd())
    }

    def nodeList(sc: EncodingProjection, nodes: SortedSet[Node], token: String): String = {
      nodes.map(n => node(sc, n, token)).mkString("")
    }

    def constraintFormat(token:String,schema:String, tableName:String, className:String,
                         identityName:String, classNameIdentity:String) = {
      s"""${token}ALTER TABLE "$schema"."${tableName}" ADD CONSTRAINT FK_$className FOREIGN KEY ($identityName) REFERENCES "$className"($classNameIdentity)"""
    }

    def constraint(sc: EncodingProjection,token: String, thisNode: Node, arc: Arc) : String = {
      val className = arc.end.name
      arc.endIdentityAlias.getKeys().map(i => constraintFormat(token,sc.encode.schema,thisNode.name,className,i.getName(),i.getName())).mkString(";")
    }

    def constraints(sc: EncodingProjection, token: String, d: Node): String = {
      val arcs = sc.p.getAdjacentArcs(d)
      arcs.filter(a1=> a1.direction != BIDIRECTIONAL).map(a => constraint(sc,token,d,a)).mkString(";\n")
    }

    def keyConstraint(sc: EncodingProjection, n:Node, token: String): String = {
      constraints(sc, indent(token,0), n)
    }

    def keyConstraints(sc: EncodingProjection, nodes: SortedSet[Node], token: String): String = {
      nodes.map(n => keyConstraint(sc, n, token)).filter(s => s.nonEmpty).mkString(";\n")
    }

    def encode(se:EncodingProjection, token: String): String = {
      encodingHeader("",se.encode.schema)+nodeList(se, se.p.getNodes(), token) +
        terminator(keyConstraints(se,se.p.getNodes(),token),";\n")
    }

    def apply(en:EncodingDefinition, p:Projection): Encoding = {
      val senc = EncodingProjection(en,p)
      senc.encoding  = encode(senc," ")
      senc
    }
}

