package mythago.plato.encoding.schema

import mythago.plato.elements.{AnIdentity, Arc, Attribute, Identity, Node}
import mythago.plato.encoding.projection.Projection
import mythago.plato.encoding.{EncodeTools, Encoding, EncodingDefinition, EncodingProjection}
import mythago.plato.{LogHelper, Platonics}

import scala.collection.Set
import scala.collection.immutable.SortedSet
import argonaut._
import Argonaut._
import mythago.plato.encoding.schema.Schema_SQL_Postgres.encode

object Schema_ArvoJson extends EncodeTools with LogHelper
{ // start of class
  // if a node is a collection
  /*

             -- Avro schema format

             {
                  "name"  : "<name>",
                  "type" : "record",
                  "namespace" : "<namespace>",
                  "fields" : [
                          <identify>
                          <attributes>

                          { "name" : <name>
                              "type" : {
                                    "type" : "record",
                                    "name" : <name>,
                                    "fields" :
                                          <multiplicity>
                                              <identify>
                                              <attributes>
                                              <nodes>

                              }
                          ]
               }
       *//* {
          "name"  : "<name>",
              "type" : "record",
              "namespace" : "<namespace>",
              "fields" : [
              *//*

        {
            "name":"children",
            "type":{
                "type": "array",
                "items":{
                    "name":"Child",
                    "type":"record",
                    "fields":[
                        {"name":"name", "type":"string"}
                    ]
                }
            }
        }

      semi BNF definition

      node ::= node [, node]
      node ::=  key : value
      key ::= node.name
      value ::= "{" node | attributeList "}"
      value ::= "[" node | attributeList "]"
      attributeList ::= attribute [, attribute]
      attribute :: "{ name :" value ", type :" the_type "}"
      identity :: attribute

      */

  val encodingStart = "["
  val encodingEnd = "]"
  val collectionStartSymbol:String = "["
  val collectionEndSymbol:String = "]"

  def associations(d: Projection, n:Node, namespace: String,
                   adjacentArcs:SortedSet[Arc],
                   traversedNodes:List[Node]): String = {
    // split associations into compositions & links

    log.debug("Associations check for loop - "+traversedNodes.mkString(" , "))
    if (traversedNodes.contains(n)) {
      log.error(" ================= Infinite loop avoided - add key instead !!!!!!!!! "+n)
      return ""
    }
     adjacentArcs.map((a2: Arc) => node( d, a2.end,
        Some(a2), traversedNodes ++ List[Node](n))).mkString(",")
  }

  def attribute(a: Attribute): String =
    " { " + quote("name") + " : " + quote(a.name) + "," + quote("type") + " : " + quote(a.valueType) + "}"

  def arrayAttribute(a: Attribute): String =
    " { " + quote("name") + " : " + quote(a.name) + "," +  quote("type") + " : " + quote("array") + ", " + quote("items") + " : " + quote(a.valueType) + "}"


  def attributes(attributeList: List[Attribute]): String = {
    // name : { type : <type>, uuid: <uuid> }
    attributeList.map((a: Attribute) => attribute(a)).mkString(",\n")
  }

  def keys(n:Node, d: Projection, keyArcs:SortedSet[Arc]): String = {
    // TODO - KEYS can be an array currently only creating a single - need to check avro standard

    keyArcs.flatten(a => if (a.multiplicity == Platonics.MANY)
        a.endIdentityAlias.getKeys.map(i => arrayAttribute(i))
      else
      a.endIdentityAlias.getKeys.map(i =>attribute(i))
    ).mkString(",\n")
  }

  def identity(id: AnIdentity): String = id.getKeys().map(attribute( _) ).mkString(",\n")

  def nodeStart(name: String, collectionStart: String, namespace: String): String = {
    String.format(
      collectionStart + lineN("{") +
      lineN(quote("name") + " : " +
       quote(name) + ",") +
      lineN( quote("type") + " : " +
       quote("record") + ",") +
      lineN( quote("namespace") + " : " +
       quote(namespace) + ",") +
      lineN(quote("fields") + " : ") +
      lineN("[") )
 }

 // end of class
 def nodeEnd(collectionEnd: String): String = lineN("]") + lineN( "}") + lineN(collectionEnd)

  def collectionStartRecord(name: String): String = {
          String.format(
          lineN("{") +
          lineN(quote("name") + " : " +
          quote(name+"_array") + ",") +
          lineN( quote("type") + " : " +
          quote("array") + ",") +
          lineN( quote("items") + " : "))
  }

 // traverse through dag and generate schema
  def node(d: Projection, n:Node, arcOption:Option[Arc], traversedNodes:List[Node]): String =
  {
    // if arc is null, then root
    val collectionStart:String = arcOption.map( a => if (a.multiplicity == Platonics.MANY)
        collectionStartRecord(n.name)
    else "").getOrElse("")
    val collectionEnd:String = arcOption.map( a => if (a.multiplicity == Platonics.MANY)
      "}" else "" ).getOrElse("")

    val adjacentArcs = d.getAdjacentArcsN2(n)

    val listOfCompositionArcs = adjacentArcs
      .filter((a1: Arc) => !traversedNodes.contains(a1.end))
      .filter(a => a.relation == Platonics.COMPOSITION)

    val listOfTerminalArcs =
      adjacentArcs
        .filter((a1: Arc) => traversedNodes.contains(a1.end))
        .filter(a => a.relation == Platonics.COMPOSITION) ++
      adjacentArcs
        .filter((a1: Arc) => !traversedNodes.contains(a1.end))
        .filter(a => a.relation != Platonics.COMPOSITION)

   String.format(
     nodeStart(n.name, collectionStart, "default") +
       identity(n.id)  +
       separator(keys( n, d, listOfTerminalArcs)) +
       separator(attributes( n.attributes)) +
       separator(associations( d,n,
         "default",listOfCompositionArcs,traversedNodes)) +
       nodeEnd(collectionEnd)
   )
  }

  def nodeList(p: Projection, nodes: Set[Node]): String = {
   nodes.map(n => node(p, n, None, List())).mkString(",")
  }

  def encode(se:EncodingProjection): String = {
    val buffer = encodingStart + nodeList(se.p,se.p.getNodes()) + encodingEnd
    Parse.parse(buffer).getOrElse(jEmptyObject).spaces2

  }

  def apply(en:EncodingDefinition, p:Projection): Encoding = {
    val senc = EncodingProjection(en,p)
    senc.encoding  = encode(senc)
    senc
  }

}

