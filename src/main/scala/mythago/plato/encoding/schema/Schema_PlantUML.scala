package mythago.plato.encoding.schema

import mythago.plato.{LogHelper, Platonics}
import mythago.plato.elements._
import mythago.plato.encoding.projection.Projection
import mythago.plato.encoding.{EncodeTools, Encoding, EncodingDefinition, EncodingProjection}

import scala.collection.Set

object Schema_PlantUML extends EncodeTools with LogHelper { // start of class
  /*
          BNF Syntax implemented

          Encoding ::= "@startuml \n" NodeList "\n" ArcList "@enduml\n"
          NodeList ::=  Node "\n" [ | NodeList ]
          Node ::=  " class " <nodeName> "{\n" AttributeList "\n}\n"
          AttributeList ::= Attribute "\n" AttributeList
          Attribute ::= <attributeName> : <type>

          ArcList ::= <Arc>  [ "\n" <ArcList> ]
          Arc ::= <nodeName1> inheritance | aggregate | link <nodeName2> : <identity>","<identity> "\n"

          inheritance ::= "<|--"
          composition ::= "*--"
          association ::= "--"
          link ::= "o--"

       */

  def encodingStart(toke: String) = "@startuml\n"

  def encodingEnd(toke: String) = "\n@enduml\n"

  def nodeStart(token: String, name: String): String = token + "class " + name + " { \n"

  // end of class
  def nodeEnd(token: String): String = token + "\n}\n"

  def attribute(token: String, a: Attribute): String =
    token + String.format(" %s : %s ", a.name, a.valueType)

  def attributes(token: String, attributes: List[Attribute]): String = { // name : { type : <type>, uuid: <uuid> }
    attributes.map((a: Attribute) => attribute(token, a)).mkString("\n")
  }

  /*
          start <-----> end (bidirectional) : this, link to other
          start ----> end (end) : this is start link to end, else exit
          start <---- end (left) : this is end link to start, else exit

          Types of Arcs supported.
          link: <id> : Class*
          multiplicity: list_<id> : List<Class *>
          composition: <id> : Class
          multiplicity: list_<id> : List<Class>

       */

  def key(token: String, thisNode: Node, arc: Arc): String = {
    val identityName = arc.endIdentityAlias.getName()
    val className = arc.end.name
    val pointer = if ((arc.relation == "link") || (arc.relation == "association")) "*" else ""
    if (arc.multiplicity == "many") " list_" + identityName + " : List[" + className + pointer + "]"
    else " " + identityName + " : " + className + pointer
  }

  def keys(p: Projection, token: String, d: Node): String = {
    val arcs = p.getAdjacentArcs(d)
    arcs.map(a => key(token,d,a)).mkString("\n")
  }

  def identity(token: String, ids: AnIdentity): String = ids.getKeys.map( attribute(token, _) ).mkString("\n")

  def node(p: Projection, d: Node, token: String): String = {
    var buffer =
      nodeStart(token, d.name) + identity(token, d.id) +
      separator(keys(p, token, d), "\n") +
      separator(attributes("", d.attributes), "\n") + //                separator(associations(d,token) ) +
      nodeEnd("")
    buffer
  }

  def association(a: Arc): String = {
    val qualifier = if (a.multiplicity == Platonics.MANY) " \"many\" "
    else ""
    a.relation match {
      case Platonics.LINK =>
        "-->" + qualifier
      case Platonics.INHERITANCE =>
        "<|--"
      case Platonics.COMPOSITION =>
        "*--" + qualifier
      case _ =>
        "---"
    }
  }

  def arc(a: Arc, token: String): String =
    a.endIdentityAlias.getKeys.map(a.start.name + " " + association(a) + " " + a.end.name + " : " + _.name).mkString("\n")

 // def arcs(d:Set[Arc], token: String): String = d.map((a1: Arc) => a1.left.matchByClass(d)).map((a: Arc) => arc(a, token)).mkString("\n")

  def nodeList(p: Projection, nodes: Set[Node], token: String): String =
    nodes.map(n => node(p, n, token)).mkString("\n")

  def arcList(p: Projection, arcs: Set[Arc], token: String): String =
    arcs.map((f: Arc) => arc(f, token)).mkString("\n")

  def encode(se:EncodingProjection, token: String): String = {
    encodingStart(token) + nodeList(se.p,se.p.getNodes(), token) +
      arcList(se.p,se.p.getArcs(), token) + encodingEnd(token)
  }


  def apply(en:EncodingDefinition, p:Projection): Encoding = {
    val senc = EncodingProjection(en,p)
    senc.encoding  = encode(senc," ")
    senc
  }

}
