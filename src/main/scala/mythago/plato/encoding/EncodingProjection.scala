package mythago.plato.encoding

import mythago.plato.encoding.projection.Projection

class EncodingProjection(ed:EncodingDefinition, val p:Projection, encoding:String="") extends Encoding(ed,encoding) {

}
object EncodingProjection {

  def apply(ed:EncodingDefinition, p:Projection):EncodingProjection = {
    new EncodingProjection(ed,p,"")
  }

  def apply(ed:EncodingDefinition, t:Projection, encoding:String):EncodingProjection = {
    new EncodingProjection(ed,t,encoding)
  }
}