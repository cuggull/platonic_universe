package mythago.plato.encoding

import mythago.plato.elements.{Arc, Element, Node}
import mythago.plato.encoding.projection.Projection

import scala.collection.mutable

abstract class Encoding(val encode: EncodingDefinition, inEncoding:String = "") {
  var encoding:String = inEncoding

  def getEncoded():String = encoding
}
