package mythago.plato.encoding.projection

import mythago.plato.Platonics.{BIDIRECTIONAL, CATEGORICAL, END, LINK, SINGLE}
import mythago.plato.elements.{Arc, Identity, Node}
import mythago.plato.encoding.Project_Graph.{createEdge, createNoOpLineage, log}
import scalax.collection.edge.LkDiEdge

import scala.collection.immutable.{Set, SortedSet}

object Project_Relational {


  // ** todo - techdebt
  // as DV needs to be moved into its own class
  // Categorfy of building new path & nodes
  // - subcategory of joins?
  def createRelationalNodes (ed:LkDiEdge[Node],a:Arc): (Set[LkDiEdge[Node]],Set[LkDiEdge[Node]]) =
  {
    log.debug("-----------createRelationalNodes Ideology Arc for : "+a.toString)
    a.direction match {
      case BIDIRECTIONAL => {
        // create new join node
        val sortedNames = SortedSet(a.start.name, a.end.name)

        val nj =
          Node("join_"+sortedNames.mkString("_"), a.start.path, Identity(sortedNames.mkString("_")+"_id") )

        val startNode = ProjectionLineage.createParentChild(a.start,a.start,LineageType.full)
        val endNode = ProjectionLineage.createParentChild(a.end,a.end,LineageType.full)

        val t1=createEdge(END, ProjectionRule(SINGLE,END,LINK,CATEGORICAL), nj, startNode._1) ++
          createEdge(END, ProjectionRule(SINGLE,END,LINK,CATEGORICAL), nj, endNode._1)

        // create a lineage from the common parents that make up the new link
        val joinArc = ProjectionLineage.createCompoundParentChild(Set[Node](a.start,a.end), nj, LineageType.arc)

        (t1,Set(startNode._2,endNode._2)++joinArc.map(t => t._2))
      }
      case _ =>
        createNoOpLineage(ed)
    }
  }

}
