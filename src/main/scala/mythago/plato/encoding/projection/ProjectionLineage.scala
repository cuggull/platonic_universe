package mythago.plato.encoding.projection

import mythago.plato.{GraphHelper, LogHelper}
import mythago.plato.elements.{Arc, Node}
import mythago.plato.encoding.projection.LineageType.{lineageType, link}

import scalax.collection.edge.Implicits.any2XEdgeAssoc
import scalax.collection.Graph
import scalax.collection.edge.LkDiEdge

object LineageType extends Enumeration {
  type lineageType = Value
  val attribute,link,identity,full,arc = Value
}

class ProjectionLineage(val family:Graph[Node,LkDiEdge])
  extends GraphHelper with LogHelper {

  def getParents() = {
    family.edges.map(e => e.toOuter._1).toSet
  }

  def getParentOf(child: Node) = {
    family.edges.filter(_.toOuter._2 == child).map(e => e.toOuter._1)
  }

  def getChildren(parent: Node): Set[(Node, lineageType)] = {
    if (family.isEmpty)
      Set[(Node, lineageType)]((parent, LineageType.full))
    else
      family.find(parent).map(n => n.edges.map(e => (e._2.toOuter,
        e.label.asInstanceOf[lineageType]))).getOrElse(Set[(Node, lineageType)]()).toSet
  }

  def getChildrenTraverser(parent: Node): Set[Node] = {
      if (family.isEmpty)
        Set[Node](parent)
      else
        family.find(parent).map(n => n.edges.map(e => e._2.toOuter)).getOrElse(Set[Node]()).toSet
    }

    def getChildType(child: Node) = {
      val x = family.find(child)
        .map(n => n.edges.map(e => e.label.asInstanceOf[lineageType]))
        .getOrElse(Set[lineageType]())
      x
    }

    def dump() = {
      log.debug(">>>>>>>>>>>>>>>>>>>> Lineage >>>>>>>>>>>>>>>>>>>>>>>>>>>")
      log.debug(dumpGraph(family))
    }
}

object ProjectionLineage {
  def createParentChild(parent:Node,
                        child:Node,
                        tag:lineageType): (Node,LkDiEdge[Node]) = {
    (child,(parent ~+#> child)(tag))
  }

  def createCompoundParentChild(parents:Set[Node],
                        child:Node,
                        tag:lineageType): Set[(Node,LkDiEdge[Node])] = {
    parents.map(pn => (child,(pn ~+#> child)(tag)))
  }

  def apply(g:Graph[Node,LkDiEdge]) = {
    new ProjectionLineage(g)
  }

  def apply() ={
    new ProjectionLineage(Graph.empty)
  }
}