package mythago.plato.encoding.projection

import mythago.plato.{LogHelper, Platonics}
import mythago.plato.Platonics._
import mythago.plato.elements.Node

class ProjectionRule(val node_regexp_start: String, val node_regexp_end: String, val depth_check: Int, val multiplicity: String, val direction: String, val relation: String, val ideology: String, val endIdentityAlias: String, val startIdentityAlias: String) extends LogHelper
  {
    def this(ruleConf:Map[String,String]) =
    {
      this(ruleConf.getOrElse(START, ""),
        ruleConf.getOrElse(END, ""),
        ruleConf.getOrElse(DEPTH_CHECK, "100000000").toInt,
        ruleConf.getOrElse(MULTIPLICITY, SINGLE),
        ruleConf.getOrElse(DIRECTION, END),
        ruleConf.getOrElse(RELATION, LINK),
        ruleConf.getOrElse(IDEOLOGY, CATEGORICAL),
        ruleConf.getOrElse(START_IDENTITY_ALIAS, ""),
        ruleConf.getOrElse(END_IDENTITY_ALIAS, ""))
    }

    def checkMatch(field: String, wildcard: String): Boolean =
    {
      if (wildcard == Platonics.WILDCARD) true
      else if (field == wildcard) true
      else false
    }

    def checkRule(start:Node,end:Node): Boolean =
    {
        if ((checkMatch(start.name, node_regexp_start) &&
          checkMatch(end.name, node_regexp_end))
          || (checkMatch(end.name, node_regexp_start) &&
          checkMatch(start.name, node_regexp_end)))
          true
        else
          false
    }

    def dump(indent:String): String =
    {
      indent + "RULE:: checkType=" + depth_check + ", Multiplicity="+ multiplicity +
      ", Direction=" + direction + ", Association=" + relation +  ", Ideology=" + ideology +"\n" +
      indent + indent + "("+ node_regexp_start + ", " + node_regexp_end + ")\n" +
      indent + indent + "("+ endIdentityAlias + ", " + startIdentityAlias + ")\n"
    }
}

object ProjectionRule {

  def apply(multiplicity: String,direction: String,relation: String,ideology: String):ProjectionRule = {

    new ProjectionRule(null, null, 100000000, multiplicity, direction, relation, ideology, null, null)
  }

}