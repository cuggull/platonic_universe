package mythago.plato.encoding.projection

import mythago.plato.{GraphHelper, LogHelper}
import mythago.plato.elements.{Arc, Identity, Node, Path, Universe}
import scalax.collection.{Graph}
import scalax.collection.edge.Implicits._
import scalax.collection.edge.LkDiEdge

import scala.collection.immutable.Set
import scala.collection.immutable.SortedSet

class Projection(val p:ProjectionRuleSet,
                 val g:Option[Graph[Node,LkDiEdge]],
                 val lineage:ProjectionLineage)
  extends GraphHelper with LogHelper
{

  def getRoot(): Option[Node] = {
    getNodeByName(p.fromNodeName)
  }

  def getNodeByName(name: String): Option[Node] = {
    g.flatMap(g1 => g1.nodes.toOuter.find(n => n.name == name))
  }

  def getNodes(): SortedSet[Node] =
    SortedSet[Node]() ++ g.map(x => x.nodes.map(n => n.toOuter).toSet).getOrElse(Set[Node]());

  def getInnerNodes() =
    g.map(x => x.nodes.map(x2 => x2.self).map(x3 => x3.edges)).getOrElse(Set());

  def getAdjacentNodes(n: Node): SortedSet[Node] = SortedSet[Node]() ++ g.get.get(n).edges.map(y => y.toOuter.to).toSet

  def getAdjacentArcs(n: Node): SortedSet[Arc] = {
    SortedSet[Arc]()  ++ g.map(g1 => g1.get(n).edges.map(y => y.toOuter.label.asInstanceOf[Arc]).toSet)
        .getOrElse(Set()).filter(a1 => a1.end.name != n.name)
  }

  def getAdjacentArcsN2(n: Node): SortedSet[Arc] = {
    val x = SortedSet[Arc]()  ++ g.map(g1 => g1.get(n).edges.filter(a1 => a1.target != n)
      .map(y => y.toOuter.label.asInstanceOf[Arc]).toSet)
      .getOrElse(Set())

    log.debug(">>>>>>>> getAdjacentArcsN2 == " + n.toString)
    log.debug(x.mkString("\n"))

    x
  }

  def getArcs(): SortedSet[Arc] = {
    SortedSet[Arc]() ++  g.map(x => x.edges.map(e => e.toOuter.label.asInstanceOf[Arc]).toSet)
      .getOrElse(Set[Arc]())
  }

  //
  def dump(): String = {
    // g.map( gr => dump(gr) ).getOrElse("")
    getNodes.mkString("\n")
  }

  def dumpGraph(): String = {
    dumpGraph(g)
  }
}


object Projection {

}
