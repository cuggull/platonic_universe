package mythago.plato.encoding.projection

import mythago.plato.Platonics.{BIDIRECTIONAL, CATEGORICAL, DATAVAULT, END, LINK, MANY, RELATIONAL}
import mythago.plato.elements.{Arc, Node}
import mythago.plato.encoding.Project_Graph.{buildGraph, createEdge, createNoOpLineage, createRelationalNodes, log}
import scalax.collection.Graph
import scalax.collection.edge.LkDiEdge

import scala.collection.immutable.Set

object Project_Datavault {

  def applyIdeology(p:Projection, g:Graph[Node,LkDiEdge]):
(Option[Graph[Node,LkDiEdge]], ProjectionLineage) =
{
  log.debug("-- Applying Ideology over Projection "+p.p.name+" --")
  g.edges.foreach( e => log.debug(e.toOuter.label.asInstanceOf[Arc].dump()))

  val idgArcs = g.edges.map( e =>
    e.toOuter.label.asInstanceOf[Arc] match {
      case a:Arc => a.ideology match {
        case DATAVAULT => createDataVaultNodes(e.label.asInstanceOf[Arc])
        case RELATIONAL => createRelationalNodes(e.toOuter,e.label.asInstanceOf[Arc])
        case _ => createNoOpLineage(e.toOuter)
      }
      case x:AnyRef => {
        log.error("Ooops its a " + x.getClass)
        (Set[LkDiEdge[Node]](), Set[LkDiEdge[Node]]())
      }
    }
  )
  (Some(buildGraph(idgArcs.flatMap(s => s._1))),
    ProjectionLineage(buildGraph(idgArcs.flatMap(s => s._2))))
}


  //  ** techdebt
  // Currently Data Vault function in projection
  // this needs to be removed so ideologies can be extended and custom built
  //
  // Return type ( x, y )
  // x -> graph == set of Arcs
  // y -> LineageRep == set of Arcs with type
  //
  // category of building new nodes and paths wthin the same parent entity
  def createDataVaultNodes(a:Arc): (Set[LkDiEdge[Node]],Set[LkDiEdge[Node]]) = {

    log.debug("-----------createDataVaultNodes Ideology Arc for : "+a.toString)

    // Create Hub, Sattelite, Link
    // create lineage from left node
    // val lineage: LineageRepo = new LineageRepo()
    // create name - repo can specialise for datavault also for custom name creation

    val hubL = ProjectionLineage.createParentChild(a.start,
      Node("hub_"+a.start.name, a.start.path, a.start.id),LineageType.identity)

    val satL =ProjectionLineage.createParentChild(
      a.start, Node("sat_"+a.start.name, a.start.path, a.start.id, a.start.attributes),
      LineageType.attribute)

    val linkL = ProjectionLineage.createParentChild(
      a.start, Node("link_"+a.start.name, a.start.path, a.start.id),
      LineageType.link)

    val hubR =ProjectionLineage.createParentChild(
      a.end,
      Node("hub_"+a.end.name, a.end.path, a.end.id),
      LineageType.identity)

    val satR = ProjectionLineage.createParentChild(
      a.end, Node("sat_"+a.end.name, a.end.path, a.end.id, a.end.attributes),
      LineageType.attribute)

    val linkR = ProjectionLineage.createParentChild(
      a.end, Node("link_"+a.end.name, a.end.path, a.end.id),
      LineageType.link)

    val datavaultProjectionRule =  ProjectionRule(MANY,END,LINK,CATEGORICAL)

    val result = createEdge(END,datavaultProjectionRule,satL._1,hubL._1) ++
      createEdge(END,datavaultProjectionRule,linkL._1,hubL._1) ++
      createEdge(END,datavaultProjectionRule,satR._1,hubR._1) ++
      createEdge(END,datavaultProjectionRule,linkR._1,hubR._1) ++
      (a.direction match {
        case start =>
          createEdge (END,datavaultProjectionRule,linkL._1,hubR._1)
        case END =>
          createEdge (END,datavaultProjectionRule,linkR._1,hubL._1)
        case BIDIRECTIONAL => {
          val t1=createEdge(END, datavaultProjectionRule, linkL._1, hubR._1) ++
            createEdge(END, datavaultProjectionRule, linkR._1, hubL._1)
          t1
        }
      })
    (result,Set(satL._2,hubL._2,linkL._2,satR._2,linkR._2,hubR._2))
  }


}
