package mythago.plato.encoding


import mythago.plato.Platonics._
import mythago.plato.{GraphHelper, LogHelper, Platonics}
import mythago.plato.elements.{Arc, Identity, Node, Path, Universe}
import scalax.collection.Graph
import scalax.collection.edge.Implicits._
import scalax.collection.edge.LkDiEdge

import scala.collection.immutable.Set
import scala.collection.immutable.SortedSet
import mythago.plato.encoding.projection._

package object Project_Graph  extends GraphHelper with LogHelper {

  def project(uni:Universe, projRS: ProjectionRuleSet): Projection = {
    // iterate over the graph
    // for each arc test a match against the rule
    log.debug("---------------- Entered project ---------------------")
    val arcResults = projRS.rules.map(r => projectByRule(r,uni) )
      .fold((Set[LkDiEdge[Node]](), Set[LkDiEdge[Node]]()))( (s, u) => (s._1 ++ u._1, s._2 ++ u._2) )
    new Projection(projRS, Some(Graph.from(null, arcResults._1)),
      ProjectionLineage(Graph.from(null, arcResults._2)) )
  }

  def projectByRule(r: ProjectionRule, uni:Universe): (Set[LkDiEdge[Node]], Set[LkDiEdge[Node]]) = {
    val paths = getMatchingArcs(r,uni)
    val edges = paths.arcs.flatMap(e => createEdge(r, e.start, e.end)).toSet
    edges.map( e =>  createNoOpLineage(e) )
      .fold((Set[LkDiEdge[Node]](), Set[LkDiEdge[Node]]()))( (s, u) => (s._1 ++ u._1, s._2 ++ u._2) )
  }

  /*
  // take a projection ruleset - create projections + ideology
  def projectByIdeology(uni:Universe, projRS: ProjectionRuleSet): Projection = {
    // iterate over the graph
    // for each arc test a match against the rule
    log.debug("---------------- Entered projectIdeology ---------------------")
    val p = project(uni,projRS)
    val result = applyIdeology(p,p.g.getOrElse(Graph.empty))
    result._2.dump()
    new Projection(projRS, result._1, result._2)
  }

  // take a projection and apply an ideology to that graph based upon any ideology
  // attributes it has
  // in theory - you could take a new Projection Ruleset and apply it
  def projectByIdeology(p:Projection): Projection =
  {
    // iterate over the graph
    // for each arc test a match against the rule
    log.debug("---------------- Entered projectIdeology ---------------------")

    val result =  applyIdeology(p,p.g.getOrElse(Graph.empty))

    result._2.dump()

    new Projection(p.p,result._1, result._2)
  }
*/




  def createNoOpLineage(ed:LkDiEdge[Node]): (Set[LkDiEdge[Node]],Set[LkDiEdge[Node]]) ={
    val noOpLineage = ed.map(n => ProjectionLineage.createParentChild(n,n,LineageType.full)).toSet
    (Set(ed), noOpLineage.map(_._2))
  }

  /*
  Using Graph to apply the rule matches
 */

  def getMatchingArcs(r: ProjectionRule, uni:Universe):Path = {
    // if direction is START then flip the start and end filters
    val startNode:Node = Node(if (r.direction == START) r.node_regexp_end else r.node_regexp_start,uni.name)
    val endNode:Node = Node(if (r.direction == START) r.node_regexp_start else r.node_regexp_end, uni.name)

    log.debug(s"Checking for Matching Arcs ${startNode} == ${endNode} ")

    val startNodes = uni.baseUni.nodes.filter(_.toOuter.matchOn(startNode))
    val endNodes = uni.baseUni.nodes.filter(_.toOuter.matchOn(endNode))
    log.debug(s" StartNodes MATCHED ${startNode.name} == ${startNodes} ")
    log.debug(s" StartNodes MATCHED ${endNode.name} == ${endNodes} ")

    val paths = startNodes.flatMap(n1 => endNodes.map(n2 => n1.shortestPathTo(n2)))
    log.debug(s" Path Contains == ${paths}")

    val pathTraversal = paths.flatMap(p0 => p0.flatMap(p => Some(p.nodes.toSeq.take(r.depth_check) zip p.edges ) ) ).flatten
    val matchedNodes = paths.flatMap(p0 => p0.flatMap(p => Some(p.nodes.map(e => e.toOuter)))).flatten.toSeq

    val sequencedEdges = pathTraversal.map( n => Arc(n._1.toOuter, n._2.toOuter) ).toSeq

    Path(startNode, sequencedEdges)
  }

  def applyRule(r:ProjectionRule,start:Node,end:Node): Set[LkDiEdge[Node]] = {
    log.debug(s"***** Entered applyRule check Rule l=${start.name} r=${end.name} regl=${r.node_regexp_start} regr=${r.node_regexp_end} dir=${r.direction}")

    if (r.checkRule(start,end)) {
      // no new node -> but new arc created - DirectedEdge
      // need to pass start & end as they were in matching rule
      // since Universe graph is undirected, but matching rule is directed
      val pairNodes = applyDirection(r:ProjectionRule,start:Node,end:Node)

      log.debug(s"    >> matched - creating Arc (${pairNodes._1},${pairNodes._2})")
      createEdge(r,pairNodes._1,pairNodes._2)

    } else
    // failed rule match for
      Set[LkDiEdge[Node]] ()
  }

  def applyDirection(r: ProjectionRule, start: Node, end: Node): (Node,Node) = {
    // if direction is START then flip the start and end filters
    val innerRStart = if (r.direction == START) r.node_regexp_end else r.node_regexp_start
    val innerREnd = if (r.direction == START) r.node_regexp_start else r.node_regexp_end

    if (end.name == innerREnd ||  start.name == innerRStart)
      (start,end)
    else if (end.name == innerRStart)
      (end,start)
    else if ( start.name == innerREnd)
      (end,start)
    else
      (start,end)
  }


  def createEdge(r:ProjectionRule,start:Node,end:Node): Set[LkDiEdge[Node]] = {
    createEdge(r.direction,r,start,end)
  }

  def createEdge(direction:String,
                 r:ProjectionRule,
                 start:Node,
                 end:Node): Set[LkDiEdge[Node]] =
  {
    direction match {
      case END | START => {
        val a = Arc(start, end, r)
        log.debug(s"createEdge - $direction : ${a.start.name} -> ${a.end.name} ")
        Set((a.start ~+#> a.end)(a))
      }
      case BIDIRECTIONAL => {
        val arcs = Arc.ArcPair(start,end,r)
        log.debug(s"createEdge - $BIDIRECTIONAL : "+
          arcs.map(a => a.dump("")).mkString(","))
        arcs.map(a => (a.start ~+#> a.end)(a))
      }
      case x:AnyRef => {
        log.error(s"Its bullshit value for direction $x")
        Set()
      }
      case null => {
        log.error(" Error parsing null Arc")
        Set()
      }
    }
  }

}
