package mythago.plato.encoding.projection

import mythago.plato.LogHelper
import mythago.plato.elements._
import mythago.plato.encoding.{Encoding, EncodingDefinition, EncodingProjection}

import scala.collection.{Set, mutable}

class IdeologyTools(encode:EncodingDefinition, proj:Projection)
  extends EncodingProjection(encode,proj) with LogHelper
{
  val joinTableMap = new mutable.HashMap[String,Int]()
  /*
        Add an element to the list unless it already exists, use the custom match function
        Set + Add function
   */

  def uniqueSet(inSet:Set[Arc], a:Arc): Boolean = {
    if (inSet.count(a0 => Arc.biDirectionalMatch(a0,a)) > 0) false else true
  }

  // using a Mutable set which is crap, but wasted too much time on this
  def joinTabRec(arcs:Set[Arc]): Set[Arc] =
  {
    val mArcSet = new mutable.HashSet[Arc]()
    arcs.foreach(a =>  if (uniqueSet(mArcSet,a)) {mArcSet.add(a)})
    val arcsSet2 = arcs.filter(a =>  uniqueSet(mArcSet,a))

    mArcSet.toSet
  }

  def getName(name:String):String = {
    if (joinTableMap.contains(name) && joinTableMap(name) > 0) {
      name + "_" + joinTableMap(name)
    } else
      name
  }

  def putNextName(name:String): Unit = {
    if (joinTableMap.contains(name)) {
      joinTableMap.put(name, joinTableMap(name)+1)
    } else
      joinTableMap.put(name,0)
  }

  /**
  def getNextName(name:String, a:Arc):String = {
    putNextName(name)
    // store the detail as an ArcDetail
    val newName = getName(name)
    arcDetails.put(newName,EncodingDetail[Arc](a,
      "join",
      "tableName",
      newName))
    newName
  } */
}

