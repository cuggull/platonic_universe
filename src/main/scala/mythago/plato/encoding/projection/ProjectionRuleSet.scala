package mythago.plato.encoding.projection

import mythago.plato.LogHelper
import mythago.plato.encoding.EncodingDefinition


class ProjectionRuleSet (
  val name:String,
  val outputEncodings: Set[EncodingDefinition],
  val rules: Set[ProjectionRule],
  val universeName:String,
  val fromNodeName:String,
  val projectionName:String,
  val applyIdeology:Boolean,
  val globalIdeology:String) extends LogHelper
{
  def this(name:String){
    this(name,null,null,null,null,null,false,null)
  }
   def dump(indent:String): String ={
     indent + ">>>>>PROJECTION "+ "[" + name + "], Universe [" +
       universeName + "] ApplyIDG="+ applyIdeology +" From="+ fromNodeName + ", " + projectionName + "\n" +
       rules.foldLeft("") { (s,r) => s+r.dump(indent)} +
       outputEncodings.foldLeft(""){ (s,e) => s+e.dump(indent)+"\n" } +"\n"
  }
}

object ProjectionRuleSet {
  def apply(name:String,
            rsEncodings:Set[EncodingDefinition],
            rsRuleSet:Set[ProjectionRule],
            rsUniverse:String,
            rsFrom:String,
            rsProjectionName:String,
            rsApplyIdeology:String,
            rsIdeology:String):ProjectionRuleSet =
  {
    new ProjectionRuleSet(name,
      rsEncodings,rsRuleSet,
      rsUniverse,rsFrom,
      rsProjectionName,rsApplyIdeology match {
        case "true" => true
        case _ => false},rsFrom)
  }
}