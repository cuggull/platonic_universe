package mythago.plato.encoding

import mythago.plato.Platonics._
import mythago.plato.elements.{Arc, Element, Node}

class EncodingDefinition(val encodingType:String,
                         val destinationURI:String,
                         val structure:String,
                         val schema:String)
{
  def this(confMap:Map[String,String]){
    this(confMap.getOrElse(ENCODING,UNKNOWN),
      confMap.getOrElse(URI,UNKNOWN),
      confMap.getOrElse(STRUCTURE,DEFAULT),
      confMap.getOrElse(SCHEMA,"public")
    )
  }

  def dump(indent: String) : String = {
    s"${indent}Encoding($encodingType), URI= $destinationURI,  $structure, $schema"
  }
}
