package mythago.plato.encoding

import mythago.plato.transform.Transform

class EncodingTransform(ed:EncodingDefinition,
                        val t:Transform,
                        encoding:String="") extends Encoding(ed,encoding) {

}

object EncodingTransform {

  def apply(ed:EncodingDefinition, t:Transform):EncodingTransform = {
    new EncodingTransform(ed,t,"")
  }

  def apply(ed:EncodingDefinition, t:Transform, encoding:String):EncodingTransform = {
    new EncodingTransform(ed,t,encoding)
  }
}