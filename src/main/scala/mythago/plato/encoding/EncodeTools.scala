package mythago.plato.encoding

import mythago.plato.LogHelper

trait EncodeTools extends LogHelper
{

  def terminator(buffer:String,end:String):String = {
    if (buffer.length > 0)
      buffer + end
    else
      ""
  }

  def separator(value: String, delimiter: String): String = {
    (if (value.trim.isEmpty) "" else delimiter) + value
  }

  def separator(value: String): String = separator(value, ",\n")

  def separator(token:String, offset:Int, value: String, delimiter:String): String = separator(indent(token,offset)+value, delimiter)

  def indent(token: String, count: Integer): String = {
    var buf = token
    if (count > 0) for (i <- 0 until count) {
      buf = buf.concat(" ")
    }
    if (count < 0 && (buf.length + count) >= 0) buf = buf.substring(0, token.length + count)
    buf
  }

  def line(token: String, buffer: String): String = line(token, 0, buffer, "")

  def line(token: String, count: Integer, buffer: String): String =
    line(token, count, buffer, "")

  def lineN(token: String, count: Integer, buffer: String): String =
    if (buffer.length != 0)
      line(token, count, buffer, "\n")
   else
      buffer

  def lineN(buffer: String): String =
    if (buffer.length != 0)
      line("", 0, buffer, "\n")
    else
      buffer

  def line(token: String, count: Integer, buffer: String, el: String): String =
    separator(indent(token, count) + buffer, el)

  def quote(buffer: String): String = "\"" + buffer + "\""

  def dot(s1:String, s2:String):String = s""""${s1}"."${s2}""""
  def dot(s1:String, s2:String, s3:String):String = s""""${s1}"."${s2}"."${s3}""""

}
