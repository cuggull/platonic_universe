package mythago.plato.elements

trait AnIdentity {
  def getKeys(): Seq[Identity]
  def getName(): String
}
