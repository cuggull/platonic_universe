package mythago.plato.elements

import mythago.plato.Platonics
import mythago.plato.encoding.projection.ProjectionRule

import java.util.Objects.hash
import scala.collection.immutable.List


class Node(val name: String, val path: String, val id: AnIdentity,
           val attributes: List[Attribute] = List[Attribute] ())
  extends Element with Ordered[Node] {

  def getId() = id.getName()

  override def toString = s"${path}.${name}.${id.getName}"

  // return 0 if the same, negative if this < that, positive if this > that
  override def compare (that: Node): Int = {
    if (this.toString == that.toString)
      0
    else if (this.toString > that.toString)
      1
    else
      -1
  }

  override def equals(that: Any): Boolean =
    that match {
      case that: Node => that.toString == this.toString
      case _ => false
    }

  override def hashCode:Int = {
    return hash(this.toString)
  }

  def matchOn(that:Any):Boolean = {
    that match {
      case that: Node => {
        if (that.name == Platonics.WILDCARD)
          true
        else if (that.name == this.name)
          true
        else if ( that.name.split(",").map(_.trim).contains(this.name) )
          true
        else
          false
      }
      case _ => false
    }
  }
  override def dump(indent:String) : String = {
    indent + this.toString + "::" +
      attributes.foldLeft(indent+indent)((s, a) => s+"\n"+a.dump(indent+indent) )
  }

}

object Node {

  def startNode(pr:ProjectionRule,uni:Universe) :Node = {
    apply(pr.node_regexp_start, uni.name)
  }

  def endNode(pr:ProjectionRule,uni:Universe) :Node = {
    apply(pr.node_regexp_end, uni.name)
  }

  def apply(name:String, path:String, id:AnIdentity, attribs: List[Attribute] ):Node = {
    new Node(name, path, id, attribs)
  }

  def apply(name:String, path:String, attribs: List[Attribute] ):Node = {
    new Node(name, path, Identity(name+"_id"), attribs)
  }

  def apply(name:String, path:String, id:String, attribs: List[Attribute] ):Node = {
    apply(name, path, Identity(id), attribs)
  }

  def apply(name:String, path:String, id:AnIdentity): Node = {
    apply(name,path,id,List[Attribute]())
  }

  def apply(name:String, path:String, id:String): Node = {
    apply(name,path,Identity(id),List[Attribute]())
  }

  def apply(name:String, path:String): Node = {
    apply(name,path,List[Attribute]())
  }

  def apply(name:String): Node = {
    apply(name,"",List[Attribute]())
  }

}
