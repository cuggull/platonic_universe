package mythago.plato.elements

import java.util.UUID

class Identity(name:String,
    valueType:String,
    defaultValue:String,
    security: String,
    format: String,
    tags: String,
    uid: UUID)
  extends Attribute(name,valueType,defaultValue,security,format,tags,uid) with AnIdentity
{
  def getKeys():Seq[Identity] = {
    Seq[Identity](this)
  }
  def getName():String = name
}

object Identity {
  def apply(name:String) = {
    new Identity(name,"key","",null,null,null, UUID.randomUUID())
  }

}
