package mythago.plato.elements

import java.util.UUID

import mythago.plato.Platonics

import scala.collection.mutable

case class Attribute( name:String,
                 valueType:String,
                 defaultValue:String,
                 security: String,
                 format: String,
                 tags: String,
                 uid: UUID) extends Element {

  override def dump(indent:String): String ={
    indent+name+"::"+valueType
  }

}

object Attribute {

  def apply(name:String): Attribute = {
    new Attribute(name,Platonics.STRING,null,null,null,null,UUID.randomUUID())
  }

  def apply(name:String, attributType:String): Attribute = {
    new Attribute(name,attributType,null,null,null,null,UUID.randomUUID())
  }

  def apply (name:String,
            attributeType:String,
            defaultValue:String,
            security: String,
            format: String,
            tags:String) =
  {
    new Attribute(name,attributeType,defaultValue,security,format,tags,UUID.randomUUID())
  }

  def apply (input:mutable.Map[String,Object]): Attribute = {
     apply (
      input.getOrElse(Platonics.NAME, "").toString,
      input.getOrElse(Platonics.TYPE, "").toString,
      input.getOrElse(Platonics.VALUE, "").toString,
      input.getOrElse(Platonics.SECURITY, "").toString,
      input.getOrElse(Platonics.STRUCTURE, "").toString,
      input.getOrElse(Platonics.TAGS, "").toString)
  }

}
