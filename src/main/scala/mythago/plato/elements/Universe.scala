package mythago.plato.elements


import scalax.collection.Graph
import scalax.collection.GraphPredef._
import scalax.collection.GraphEdge._

class Universe( val name:String,
                val attribs: Map[String, Set[Attribute]],
                val nodes: Map[String, Node],
                val arcs: Set[Arc]) extends Element
{
  val baseUni: Graph[Node,UnDiEdge] = {
    val arcList = arcs.map(a => a.start ~ a.end).toList
    val nodeList = nodes.values.toList
    Graph.from(nodeList,arcList)
  }

  override def equals(obj: scala.Any): Boolean = {
    obj match {
      case other: Universe => other.name == name
      case _ => false
    }
  }

  def this(name:String) {
    this(name, null, null, null)
  }

  override def ++ : Universe = ???
  /*
     inner_left
     outer_left
     inner_right
     outer_right
     inner -- intersection
     outer -- full outer join
   */
  override def merge(policy:String) = ???

  override def dump(indent:String): String = {
    indent + ">>> Universe::"+name+" <<<\n"+
      indent + baseUni.mkString("\n"+indent)
  }


  def dump_original(indent:String): String = {

    indent + ">>> Universe::"+name+" <<<\n"+
      indent + "---- " + "NODES \n" +
      nodes.foldLeft("") {
        case ( s, (k, v)) => s + v.dump(indent+indent) + "\n"
      } +
      indent + "---- " + "ARCS\n" +
        arcs.foldLeft("") {
        case ( s,a) => s + a.dump(indent+indent) + "\n"
      }
  }

  def getUndirectedGraph(): Unit = {

    // declare graph structure

    // add nodes and arcs


    //
  }
}
