package mythago.plato.elements

trait Element {

  def ++ : Universe = ???

  /*
     inner_left
     outer_left
     inner_right
     outer_right
     inner -- intersection
     outer -- full outer join
   */
  def merge(policy:String) = ???

  def dump(indent:String) : String = ???

}
