package mythago.plato.elements

import mythago.plato.LogHelper
import mythago.plato.Platonics._
import mythago.plato.encoding.projection.ProjectionRule
import scalax.collection.GraphEdge

case class Arc(start: Node,
               end: Node,
               multiplicity:String,
               direction: String,
               relation: String,
               ideology: String,
               endIdentityAlias: AnIdentity,
               startIdentityAlias: AnIdentity)
  extends Element with Ordered[Arc]
{
  override def equals(other: Any) = other match {
    case that: Arc => (this.toString == that.toString)
    case _  => false
  }

  override def hashCode = (this.toString).##

  override def dump(indent: String=""):String =
  {
    s"$indent [${this.toString}]"
  }

  override def toString = s"${start.name},${end.name},${direction},${relation},${multiplicity},${ideology},${startIdentityAlias.getName()},${endIdentityAlias.getName()}"

  // return 0 if the same, negative if this < that, positive if this > that
  def compare (that: Arc): Int = {
    if (this.toString == that.toString)
      0
    else if (this.toString > that.toString)
      1
    else
      -1
  }

  def contains(n:Node): Boolean ={
    if (n == start || n == end) true else false
  }

}

object Arc extends LogHelper {
  val validDirections:Set[String] = Set(START,END,BIDIRECTIONAL,UNKNOWN)
  val validMultiplicty:Set[String] = Set(SINGLE,MANY,UNKNOWN)
  val validAssociation:Set[String] = Set(LINK,COMPOSITION,UNKNOWN,INHERITANCE)
  val validIdeology:Set[String] = Set(DATAVAULT, RELATIONAL, CATEGORICAL)


  def constrainValue(in:String, validSet:Set[String]):String = {
    log.debug(s" constraining '$in' against $validSet")
    if (validSet.contains(in.toLowerCase)) in
    else {
      log.warn(s"Arc Unknown Attribute = '${in}' using ${validSet.head}")
      // return first element as default - this will one day cause a mysterious bug FYI
      validSet.head
    }
  }

  def ArcPair(start:Node, end:Node, r:ProjectionRule) : Set[Arc] = {
    Set(Arc(start,end,r.multiplicity, r.direction, r.relation, r.ideology,
      r.endIdentityAlias, r.startIdentityAlias)) ++
      Set(Arc(end,start,r.multiplicity, r.direction, r.relation, r.ideology,
      r.startIdentityAlias, r.endIdentityAlias))
  }

  def biDirectionalMatch(a1:Arc, a2:Arc):Boolean = {
    log.debug(s"biDirectionalMatch Comparing (${a1.endIdentityAlias.getName()},${a1.startIdentityAlias.getName()}) (${a2.endIdentityAlias.getName()},${a2.startIdentityAlias.getName()})")
    if (a1.direction != BIDIRECTIONAL || a2.direction != BIDIRECTIONAL) return false
    a1 match {
      case that: Arc => {
        if (that.endIdentityAlias.getName() == a2.startIdentityAlias.getName()
          && that.startIdentityAlias.getName() == a2.endIdentityAlias.getName()
          && that.direction == a2.direction
          && that.relation == a2.relation
          && that.multiplicity == a2.multiplicity )
        {
          log.debug(" >>>>>>>>>>> MATCHED")
          true
        }
        else
          false
      }
      case _  => false
    }
  }

  def apply(start:Node, end:Node, multiplicity:String,
            direction: String, association:String, ideology:String,
            endIdentityAlias: String,
            startIdentityAlias: String): Arc =
  {
/*  Removing the flipping of start & end dude to direction
    instead have functions to get actuals so as to not lose information
    val actualStart = { if (direction == END) start else end}
    val actualEnd = { if (direction == END) end else start} */
    val actualStart = start
    val actualEnd = end
    val keyLabelEnd:AnIdentity = endIdentityAlias match {
      case null |  "" =>
        actualEnd.id
      case _ =>
        Identity(endIdentityAlias)
    }
    val keyLabelStart:AnIdentity = startIdentityAlias match {
      case null |  "" =>
        actualStart.id
      case _ =>
        Identity(startIdentityAlias)
    }
    new Arc(actualStart,actualEnd,
      constrainValue(multiplicity,validMultiplicty),
      constrainValue(direction,validDirections),
      constrainValue(association,validAssociation),
      constrainValue(ideology,validIdeology),
      keyLabelEnd,keyLabelStart)
  }

  def apply(start:Node, end:Node, r:ProjectionRule):Arc = {
     Arc(start,end,r.multiplicity, r.direction, r.relation, r.ideology,
      r.endIdentityAlias, r.startIdentityAlias)
  }

  def apply(start:Node, end:Node): Arc = {
     Arc(start,end,SINGLE,END,LINK,CATEGORICAL,end.id,start.id)
  }

  def apply(in_start:String, in_end:String): Arc = {
    val start = Node(in_start)
    val end = Node(in_end)
    Arc(start,end,SINGLE,END,LINK,CATEGORICAL,end.id,start.id)
  }

  def apply(in_start:String, in_end:String, in_endIdentityAlias:String): Arc = {
    val start = Node(in_start)
    val end = Node(in_end)
    val end_id = Identity(in_endIdentityAlias)
    Arc(start,end,SINGLE,END,LINK,CATEGORICAL,end_id,start.id)
  }

  def apply(ge:GraphEdge.UnDiEdge[Node]): Arc  = {
    Arc(ge._1, ge._2)
  }

  def apply(n: Node,ge:GraphEdge.UnDiEdge[Node]): Arc  = {
    if (n == ge._1)
      Arc(ge._1, ge._2)
    else if (n == ge._2)
      Arc(ge._2, ge._1)
    else {
      log.warn(s"Arc: direction Node ${n} does not match within Arc ${ge} defaulting")
      Arc(ge._1, ge._2)
    }

  }

  def apply():Arc = {
    Arc(Node(""),Node(""))
  }

/*
  implicit def orderingByName[A <: Arc]: Ordering[A] =
    Ordering.by(e => (e.start.name, e.end.name))

  val orderingById: Ordering[Arc] = Ordering.by(e => e.end.name)
*/
}
