package mythago.plato.elements

import java.util.UUID
import java.util.zip.CheckedOutputStream

class CompoundIdentity(val keys:Seq[Identity]) extends AnIdentity {
  def getKeys(): Seq[Identity] = keys
  def getName: String = {
    keys.map(id => id.name).mkString("_")
  }
}

object CompoundIdentity {
  def apply(keys:Seq[Identity]):  CompoundIdentity = {
    new CompoundIdentity(keys)
  }
}
