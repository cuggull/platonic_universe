package mythago.plato.elements

import mythago.plato.LogHelper
import mythago.plato.encoding.projection.ProjectionLineage

class Path(
            val start:Node,
            val arcs:Seq[Arc],
            val lineage: ProjectionLineage)
  extends LogHelper {
  def dump(): String = {
   s" ${start.name},${lineage.getChildType(start)} :: " + arcs.map(a => s" ${a.dump("")}").mkString(" , ")
  }

  def hasParent(they: Node): Boolean = {
    lineage.getParents().contains(they)
  }
}

object Path {
  def apply ( startNode:Node,
              paths: Seq[Arc],
              lineage: ProjectionLineage):Path = {
    new Path(startNode,paths,lineage)
  }
  def apply ( startNode:Node,
              paths: Seq[Arc]):Path = {
    new Path(startNode,paths,ProjectionLineage())
  }
}