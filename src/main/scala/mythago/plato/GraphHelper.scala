package mythago.plato

import mythago.plato.elements.Node
import scalax.collection.Graph
import scalax.collection.edge.LkDiEdge

import scala.collection.Set

trait GraphHelper {


  // this function was built for debugging why graph was creating duplicates
  def buildGraph(arcSet : Set[LkDiEdge[Node]]) : Graph[Node,LkDiEdge] = {
    if (arcSet.isEmpty )
      Graph.empty
    else {
      // todo - remove cast at some point put this cast in only because intellij couldnt handle this return
      (buildGraph(arcSet.tail) + arcSet.head).asInstanceOf[Graph[Node,LkDiEdge]]
    }
  }

  def dump(g:Graph[Node,LkDiEdge]):String = {
    g.nodes.map( n => n.toOuter  ).mkString(",\n")
    // n.edges.map(e => )
  }

  def dumpGraph(g:Option[Graph[Node,LkDiEdge]]): String = {
    val gr = g.getOrElse(Graph.empty)
    dumpGraph(gr)
  }

  def dumpGraph(g:Graph[Node,LkDiEdge]): String = {
    "Node count =" + g.nodes.toList.length + "\n" + "Arc count =" + g.edges.toList.length + "\n " + g.mkString(" \n ")
  }


}
