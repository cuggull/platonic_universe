package mythago.plato

import mythago.plato.elements._
import mythago.plato.meta.{Meta, MetaHOCON}
import mythago.plato.Platonics._
import mythago.plato.encoding.schema._

import java.nio.file._
import mythago.plato.encoding.cmd.{Cmd_DSL, Cmd_SQL_Postgres, Cmd_SQL_Postgres_opt2}
import mythago.plato.encoding.projection.{Projection, ProjectionRuleSet}
import mythago.plato.encoding.{Encoding, EncodingDefinition, EncodingTransform, Project_Graph}
import mythago.plato.encoding.schema.Schema_PlantUML
import mythago.plato.transform.{Transform, TransformDefinition}

import java.io.File

// session is the main control around plato contexts

class Session extends LogHelper {
  var conf: Option[Meta] = None
  var unis =  Map[String, Universe]()
  var projectionRuleSets = Map[String, ProjectionRuleSet]()
  var encodingOutputs = Set[EncodingDefinition]()
  var transformDefinitions = Map[String, TransformDefinition]()

  def setConfig(config:Meta): Unit = {
    conf = Some(config)
  }

  def loadUniverse(): Unit = {
    if (! conf.isDefined) {
      log.warn("Can't load universe Config is not defined")
      return
    }
    // first try and load globals
    val globals = conf.get.getGlobals
    unis = conf.get.getUniverses
  }

  def loadProjection(): Unit = {
    projectionRuleSets = conf.get.getProjectionRuleSets
  }

  def loadTransforms(): Unit = {
    transformDefinitions = conf.get.getTransforms
  }

  def project(name:String): Option[Projection] = {
    val projRS = projectionRuleSets.get(name)
    if (!projRS.isDefined) {
      log.error(s" Projection $name not found")
      return None
    }
    if (projRS.get.applyIdeology)
      projectIdeology(name)
    else
      baseProject(name)
  }

  def baseProject(name:String): Option[Projection] = {
    val projRS = projectionRuleSets.get(name)
    if (!projRS.isDefined) {
      log.error(s" Projection $name not found")
      return None
    }

    val u = unis.get(projRS.get.universeName)
    if (!u.isDefined) {
      log.error(s" Universe ${projRS.get.universeName} not found")
      return None
    }

    Some(Project.project(u.get,projRS.get))
  }

  def projectIdeology(name:String): Option[Projection] = {
    val projRS = projectionRuleSets.get(name)
    if (!projRS.isDefined) {
      log.error(s" Projection $name not found")
      return None
    }
    val u = unis.get(projRS.get.universeName)
    if (!u.isDefined) {
      log.error(s" Universe ${projRS.get.universeName} not found")
      return None
    }

    Some(Project.projectByIdeology(u.get,projRS.get))
  }

  def buildTransform(name:String) : Option[Transform] = {
    val transDef =  transformDefinitions.get(name)
    if (!transDef.isDefined) {
      log.error(s"Transformation $name not found")
      return None
    }
    // build projections
    val srcProj = project(transDef.get.src)
    val destProj = project(transDef.get.dest)

    if (!(srcProj.isDefined && destProj.isDefined)) {
      log.error(s"Transform projections ${transDef.get.src} or ${transDef.get.dest} is not defined")
      return None
    }
    Transform.exec(srcProj.get,destProj.get,transDef.get)
  }

  def generateTransformEncoding(t:Transform) = {
    log.debug("generateTransformEncoding")
    log.debug(t.srcPaths.mkString("\n"))

    val srcEnc = generateSchemaEncodings(t.srcProjection)
    val dpEnc = generateSchemaEncodings(t.destProjection)
    val cmds = generateCmdEncodings(t,dpEnc,srcEnc)

    (cmds,srcEnc,dpEnc)
  }

  def generateCmdEncodings(t:Transform,
                           destEncodings:Map[String,Encoding],
                           srcEncodings:Map[String,Encoding]) : Map[String,Encoding] =
  {
    t.transformDefinition.outputEncodings.flatMap(e =>
      e.encodingType match {
        case POSTGRES => {
          Some(POSTGRES, Cmd_SQL_Postgres_opt2(e,t))
        }
        case PLATO_DSL => {
          Some(PLATO_DSL, Cmd_DSL(e, t))
        }
        case _ => None
      }
    ).toMap
  }

  def generateSchemaEncodings(proj:Projection) : Map[String,Encoding] = {
    proj.p.outputEncodings.flatMap(e => e.encodingType match {
        case PLANTUML => {
          Some (PLANTUML, Schema_PlantUML(e,proj))
        }
        case POSTGRES => {
          Some (POSTGRES, Schema_SQL_Postgres(e,proj))
        }
        case AVROJSON => {
          Some (AVROJSON, Schema_ArvoJson(e,proj))
        }
        case _ => None
      }
    ).toMap
  }

  def execProjection(name:String):Unit = {
    val schemata = this.project(name).map(p1 => this.generateSchemaEncodings(p1))
    this.writeEncodings(schemata.getOrElse(Map()).map(e=>e._2).toSet)
  }

  def execIdeologyProjection(name:String):Unit = {
    val schemata = this.projectIdeology(name).map(p1 => this.generateSchemaEncodings(p1))
    this.writeEncodings(schemata.getOrElse(Map()).map(e=>e._2).toSet, "idg_")
  }

  def execTransform(name:String):Unit = {
    val t = this.buildTransform(name)

    if (!t.isEmpty) {
      log.debug("---------------Generate Encoding------------------")
      val schemata = this.generateTransformEncoding(t.get)

      this.writeEncodings(schemata._1.values.toSet)
      this.writeEncodings(schemata._2.values.toSet)
      this.writeEncodings(schemata._3.values.toSet)

    } else
      log.debug(s"No Transform Found $t")
  }


  def writeEncodings(encoded:Set[Encoding], prefix:String=""): Unit = {
    encoded.foreach(e => write(e.getEncoded(), insertPrefix(e.encode.destinationURI,prefix)))
  }

  def insertPrefix(uri:String, prefix:String):String = {
    val p = Paths.get(uri)
    val dir = if (p.getParent != null) p.getParent.toString+java.nio.file.FileSystems.getDefault().getSeparator() else ""
   dir + prefix + p.getFileName.toString
  }

  def write(buf:String, file:String): Unit = {
    FileHelper.tryWriteToFile(buf,file)
  }

  def dump(): String = {
    ">>>================ Dumping Session =============<<<\n" +
      "-------- Dumping Universes ---------\n" +
    unis.foldLeft(""){
      case (f,(k,v)) => f + v.dump("     ")
    } +
    "\n-------- Dumping Projections ---------\n" +
    projectionRuleSets.foldLeft(""){
      case (f,(k,v)) => f + v.dump("     ")
    }
  }
}

object Session extends LogHelper {

  def apply():Session = {
    new Session
  }

  def initialise(configName:String):Option[Session] = {
    val confFile = new File(configName)

    if (!confFile.exists) {
      log.error(s"'${configName}' config file not found")
      return None
    }
    val conf = new MetaHOCON(confFile)
    val session = new Session()
    session.setConfig(conf)
    session.loadUniverse()
    session.loadProjection()
    session.loadTransforms()

    Some(session)
  }

}
