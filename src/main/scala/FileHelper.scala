package mythago.plato

import scala.io.Source
import scala.util.Try
import java.io.{BufferedWriter, FileWriter, File, PrintWriter}

object FileHelper {
  val defaultBufferSize: Int = 65536

  def tryUsingAutoCloseable[A <: AutoCloseable, R]
  (instantiateAutoCloseable: () => A) //parameter list 1
  (transfer: A => scala.util.Try[R]) //parameter list 2
  : scala.util.Try[R] =
    Try(instantiateAutoCloseable())
      .flatMap(
        autoCloseable => {
          var optionExceptionTry: Option[Exception] = None
          try
            transfer(autoCloseable)
          catch {
            case exceptionTry: Exception =>
              optionExceptionTry = Some(exceptionTry)
              throw exceptionTry
          }
          finally
            try
              autoCloseable.close()
            catch {
              case exceptionFinally: Exception =>
                optionExceptionTry match {
                  case Some(exceptionTry) =>
                    exceptionTry.addSuppressed(exceptionFinally)
                  case None =>
                    throw exceptionFinally
                }
            }
        }
      )

  def tryUsingSource[S <: scala.io.Source, R]
  (instantiateSource: () => S)
  (transfer: S => scala.util.Try[R])
  : scala.util.Try[R] =
    Try(instantiateSource())
      .flatMap(
        source => {
          var optionExceptionTry: Option[Exception] = None
          try
            transfer(source)
          catch {
            case exceptionTry: Exception =>
              optionExceptionTry = Some(exceptionTry)
              throw exceptionTry
          }
          finally
            try
              source.close()
            catch {
              case exceptionFinally: Exception =>
                optionExceptionTry match {
                  case Some(exceptionTry) =>
                    exceptionTry.addSuppressed(exceptionFinally)
                  case None =>
                    throw exceptionFinally
                }
            }
        }
      )

  def tryPrintToFile(
                      lines: List[String],
                      location: File,
                      bufferSize: Int = defaultBufferSize
                    ): Try[Unit] =
    tryUsingAutoCloseable(() => new FileWriter(location)) { fileWriter =>
      tryUsingAutoCloseable(() => new BufferedWriter(fileWriter, bufferSize)) { bufferedWriter =>
        tryUsingAutoCloseable(() => new PrintWriter(bufferedWriter)) { printWriter =>
          Try(lines.foreach(line => printWriter.println(line)))
        }
      }
    }

  def tryWriteToFile(
                      content: String,
                      location: String,
                      bufferSize: Int = defaultBufferSize
                    ): Try[Unit] =
    tryUsingAutoCloseable(() => new FileWriter(location)) { fileWriter =>
      tryUsingAutoCloseable(() => new BufferedWriter(fileWriter, bufferSize)) { bufferedWriter =>
        Try(bufferedWriter.write(content))
      }
    }

  def tryProcessSource(
                        file: File,
                        parseLine: (String, Int) => List[String] = (line, index) => List(line),
                        filterLine: (List[String], Int) => Boolean = (values, index) => true,
                        retainValues: (List[String], Int) => List[String] = (values, index) => values,
                        isFirstLineNotHeader: Boolean = false
                      ): Try[List[List[String]]] =
    tryUsingSource(() => Source.fromFile(file)) { source =>
      Try(
        (for {
          (line, index) <- source.getLines().buffered.zipWithIndex
          values = parseLine(line, index)
          if (index == 0 && !isFirstLineNotHeader) || filterLine(values, index)
          retainedValues = retainValues(values, index)
        } yield retainedValues
          ).toList
      )
    }
}